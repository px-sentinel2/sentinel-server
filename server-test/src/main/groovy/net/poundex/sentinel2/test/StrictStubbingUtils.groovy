package net.poundex.sentinel2.test


import org.spockframework.mock.IMockInvocation

trait StrictStubbingUtils {
	
	void unexpectedCall(Object otherDelegate) {
		IMockInvocation invocation = otherDelegate as IMockInvocation
		UnexpectedCallException ex =  new UnexpectedCallException(
				invocation, 
				new StackTraceFilter(invocation.mockObject.specification.specificationContext.currentSpec))
		throw ex
	}
	
	static class UnexpectedCallException extends AssertionError {
		UnexpectedCallException(IMockInvocation invocation, StackTraceFilter filter) {
			super((Object) "Unexpected call: ${invocation.method.name}(${invocation.arguments.join(", ")})")
			filter.filter(this)
		}
	}
}