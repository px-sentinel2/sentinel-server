package net.poundex.sentinel2.test

import groovy.transform.CompileStatic
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

import java.time.Duration

@CompileStatic
trait PublisherUtils {
	
	private static Duration TIMEOUT = Duration.ofMillis(400)
	
	public <T> T block(Mono<T> mono) {
		return doBlock(mono)
	}
	
	public <T> List<T> block(Flux<T> flux) {
		return doBlock(flux.collectList())
	}
	
	private <T> T doBlock(Mono<T> mono) {
		return mono.block(TIMEOUT)
	}
}
