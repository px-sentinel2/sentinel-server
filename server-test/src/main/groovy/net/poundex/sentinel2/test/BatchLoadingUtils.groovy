package net.poundex.sentinel2.test

import graphql.schema.DataFetchingFieldSelectionSet
import graphql.schema.SelectedField

trait BatchLoadingUtils {
	
	DataFetchingFieldSelectionSet justId() {
		return [getFields: { [field("id")] }] as DataFetchingFieldSelectionSet
	}
	
	DataFetchingFieldSelectionSet fields(String... fields) {
		return [getFields: { fields.collect(this.&field) }] as DataFetchingFieldSelectionSet
	}
	
	SelectedField field(String name) { 
		return [getName: { name }] as SelectedField
	}
}