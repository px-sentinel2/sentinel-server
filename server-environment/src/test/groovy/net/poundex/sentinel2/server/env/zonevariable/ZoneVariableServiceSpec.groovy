package net.poundex.sentinel2.server.env.zonevariable

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.device.port.PortEvents
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableEvents.ZoneVariableUpdatedEvent
import net.poundex.sentinel2.server.env.zonevariable.contributor.ZoneVariableContributor
import net.poundex.sentinel2.server.env.zonevariable.contributor.ZoneVariableContributorRepository
import net.poundex.sentinel2.server.messaging.Subscribable
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Consumer

class ZoneVariableServiceSpec extends Specification {

	private static final URI DEVICE_PORT_1_ID = "dev://dev/port1".toURI()
	private static final URI DEVICE_PORT_2_ID = "dev://dev/port2".toURI()

	private static final Zone A_ZONE = Zone.builder().build()

	DevicePort devicePort = Stub() {
		getPortId() >> DEVICE_PORT_1_ID
	}
	DevicePort anotherDevicePort = Stub() {
		getPortId() >> DEVICE_PORT_2_ID
	}
	
	Value value = Stub()
	Value anotherValue = Stub()
	Value averageValue = Stub()
	
	Value defaultValue = Stub()

	ZoneVariableEvents zoneVariableEvents = Mock()
	Consumer consumer
	PortEvents portEvents = Stub() {
		portValuePublished() >> (({ consumer = it }) as Subscribable)
	}

	ZoneVariable zoneVariable = Stub() {
		fold({ Collection c -> c.collect { it.value() }.toSet() == [value].toSet() }) >> value
		fold({ Collection c -> c.collect{ it.value() }.toSet() == [anotherValue].toSet() }) >> anotherValue
		fold({ Collection c -> c.collect { it.value() }.toSet() == [value, anotherValue].toSet() }) >> averageValue
		getDefaultValue() >> Optional.of(defaultValue)
	}
	ZoneVariableContributor zoneVariableContributor = Stub() {
		getZoneVariable() >> zoneVariable
		getZone() >> A_ZONE
	}
	ZoneVariableContributorRepository zoneVariableContributorRepository = Stub() {
		findByPortId(DEVICE_PORT_1_ID) >> Flux.just(zoneVariableContributor)
		findByPortId(DEVICE_PORT_2_ID) >> Flux.just(zoneVariableContributor)
	}
	
	@Subject
	ZoneVariableService service = new ZoneVariableService(
			portEvents, 
			zoneVariableContributorRepository, 
			zoneVariableEvents)
			.init()
	
	void "Sets and publishes new values based on contributed port values"() {
		when:
		consumer.accept(new PortEvents.PortValuePublishedEvent(devicePort, value))

		then:
		1 * zoneVariableEvents.zoneVariableUpdated(zvEvent(value))
		service.getValue(zoneVariable, A_ZONE).orElseThrow() == value

		when:
		consumer.accept(new PortEvents.PortValuePublishedEvent(devicePort, anotherValue))

		then:
		1 * zoneVariableEvents.zoneVariableUpdated(zvEvent(anotherValue))
		service.getValue(zoneVariable, A_ZONE).orElseThrow() == anotherValue

		when:
		consumer.accept(new PortEvents.PortValuePublishedEvent(anotherDevicePort, value))
		
		then:
		1 * zoneVariableEvents.zoneVariableUpdated(zvEvent(averageValue))
		service.getValue(zoneVariable, A_ZONE).orElseThrow() == averageValue
	}
	
	void "Handles explicit sets"() {
		when:
		service.setValue(zoneVariable, A_ZONE, value)
		
		then:
		1 * zoneVariableEvents.zoneVariableUpdated(zvEvent(value))
		
		expect:
		service.getValue(zoneVariable, A_ZONE).get() == value
	}
	
	void "Uses default if no current value"() {
		expect:
		service.getValue(zoneVariable, A_ZONE).orElseThrow() == defaultValue
	}
	
	private ZoneVariableUpdatedEvent zvEvent(Value value) {
		return new ZoneVariableUpdatedEvent(zoneVariable, A_ZONE, value)
	}
}
