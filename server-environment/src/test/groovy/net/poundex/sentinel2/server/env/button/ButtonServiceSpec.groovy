package net.poundex.sentinel2.server.env.button

import net.poundex.sentinel2.server.button.Button
import net.poundex.sentinel2.server.button.ButtonSource
import net.poundex.sentinel2.server.button.ButtonSourceRepository
import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.device.port.PortEvents
import net.poundex.sentinel2.server.env.action.Action
import net.poundex.sentinel2.server.env.action.ActionManager
import net.poundex.sentinel2.server.env.action.MacroAction
import net.poundex.sentinel2.server.env.value.ButtonValue
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.messaging.Subscribable
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Consumer

class ButtonServiceSpec extends Specification {
	
	private static final URI SOURCE_PORT_ID = "dev://dev/1".toURI()
	private static final Action ACTION = MacroAction.builder().build()
	private static final Button BUTTON = Button.builder().action(ACTION).build()
	private static final ButtonSource BUTTON_SOURCE = ButtonSource.builder().button(BUTTON).build()
	private static final ButtonValue BUTTON_VALUE = new ButtonValue(1, ButtonValue.Type.PUSHED)
	
	Consumer<PortEvents.PortValuePublishedEvent> listener
	PortEvents portEvents = Stub() {
		portValuePublished() >> ({ listener = it} as Subscribable)
	}
	DevicePort devicePort1 = Stub() {
		getPortId() >> SOURCE_PORT_ID
	}
	Value otherValue = Stub()
	
	DevicePort devicePort2 = Stub() {
		getPortId() >> SOURCE_PORT_ID
	}
	ButtonSourceRepository buttonSourceRepository = Mock()
	ActionManager actionManager = Mock()
	
	@Subject
	ButtonService service = new ButtonService(portEvents, buttonSourceRepository, actionManager).init()
	
	void "Runs button actions for button events"() {
		given:
		1 * buttonSourceRepository.findBySourceAndValue(SOURCE_PORT_ID, BUTTON_VALUE) >> Mono.just(BUTTON_SOURCE)
		
		expect:
		listener.accept(new PortEvents.PortValuePublishedEvent(devicePort1, otherValue))
		
		when:
		listener.accept(new PortEvents.PortValuePublishedEvent(devicePort2, BUTTON_VALUE))
		
		then:
		1 * actionManager.run(ACTION)
	}
	
	void "Runs action on activate"() {
		when:
		service.activate(Button.builder().action(ACTION).build())
		
		then:
		1 * actionManager.run(ACTION)
	}
}
