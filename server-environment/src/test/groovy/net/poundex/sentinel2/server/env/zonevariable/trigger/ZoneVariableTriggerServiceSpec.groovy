package net.poundex.sentinel2.server.env.zonevariable.trigger

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.Action
import net.poundex.sentinel2.server.env.action.ActionManager
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableEvents
import net.poundex.sentinel2.server.messaging.Subscribable
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Consumer

class ZoneVariableTriggerServiceSpec extends Specification {
	
	private static final Zone ZONE = Zone.builder().build()
	private static final Value VALUE1 = new Value() { }
	private static final Value VALUE2 = new Value() { }
	private final ZoneVariable ZONE_VARIABLE = Stub(ZoneVariable)
	private final Action ACTION1 = Stub()
	private final Action ACTION2 = Stub()
	private final Action ACTION3 = Stub()
	
	EnvironmentPredicate predicate1 = Stub() {
		test(_) >> true
	}
	EnvironmentPredicate predicate2 = Stub() {
		test(_) >> false
	}
	
	ZoneVariableTrigger triger1 = aZoneVariableTrigger(1, ACTION1)
	ZoneVariableTrigger triger2 = aZoneVariableTrigger(2, ACTION2, predicate1)
	ZoneVariableTrigger triger3 = aZoneVariableTrigger(3, ACTION3, predicate2)
	
	ZoneVariableTriggerRepository zoneVariableTriggerRepository = Mock() {
		2 * findByZoneAndZoneVariable(ZONE, ZONE_VARIABLE) >> Flux.just(triger1, triger2, triger3)
	}
	Consumer<ZoneVariableEvents.ZoneVariableUpdatedEvent> listener
	ZoneVariableEvents zoneVariableEvents = Stub() {
		zoneVariableUpdated() >> ({ listener = it } as Subscribable)
	}
	SentinelEnvironment sentinelEnvironment
	ActionManager actionManager = Mock()

	@Subject
	ZoneVariableTriggerService service = new ZoneVariableTriggerService(
			zoneVariableTriggerRepository, 
			zoneVariableEvents, 
			sentinelEnvironment, 
			actionManager)
			.init()
	
	
	void "Runs trigger actions when zone variables are updated"() {
		when:
		listener.accept(new ZoneVariableEvents.ZoneVariableUpdatedEvent(ZONE_VARIABLE, ZONE, VALUE1))

		then:
		1 * actionManager.run(ACTION1)
		1 * actionManager.run(ACTION2)
		0 * actionManager._
		
		when:
		listener.accept(new ZoneVariableEvents.ZoneVariableUpdatedEvent(ZONE_VARIABLE, ZONE, VALUE2))

		then:
		0 * actionManager._
	}
	
	private ZoneVariableTrigger aZoneVariableTrigger(int n, Action action, EnvironmentPredicate predicate = null) {
		return ZoneVariableTrigger.builder()
				.id("${n}")
				.zone(ZONE)
				.zoneVariable(ZONE_VARIABLE)
				.value(VALUE1)
				.action(action)
				.predicate(Optional.ofNullable(predicate))
				.build()
	}
}
