package net.poundex.sentinel2.server.env.zonevariable.trigger;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.env.action.ActionManager;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableEvents;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
@Slf4j
class ZoneVariableTriggerService {

	private final ZoneVariableTriggerRepository zoneVariableTriggerRepository;
	private final ZoneVariableEvents zoneVariableEvents;
	private final SentinelEnvironment sentinelEnvironment;
	private final ActionManager actionManager;

	@PostConstruct
	public ZoneVariableTriggerService init() {
		zoneVariableEvents.zoneVariableUpdated().subscribe(event ->
				zoneVariableTriggerRepository.findByZoneAndZoneVariable(event.zone(), event.zoneVariable())
						.filter(zvt -> zvt.getValue().equals(event.value()))
						.filter(zvt -> zvt.getPredicate().map(p -> p.test(sentinelEnvironment)).orElse(true))
						.subscribe(this::doTriggerTrigger));
		return this;
	}

	private void doTriggerTrigger(ZoneVariableTrigger<?, ?> zvt) {
		log.info("Triggering {}", zvt.getName());
		actionManager.run(zvt.getAction());
	}
}
