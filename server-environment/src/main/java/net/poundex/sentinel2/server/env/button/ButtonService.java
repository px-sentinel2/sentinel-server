package net.poundex.sentinel2.server.env.button;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.button.Button;
import net.poundex.sentinel2.server.button.ButtonSourceRepository;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.PortEvents;
import net.poundex.sentinel2.server.env.action.ActionManager;
import net.poundex.sentinel2.server.env.value.ButtonValue;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Service
@Slf4j
public class ButtonService {
	
	private final PortEvents portEvents;
	private final ButtonSourceRepository buttonSourceRepository;
	private final ActionManager actionManager;
	
	@PostConstruct
	ButtonService init() {
		portEvents.portValuePublished().subscribe(this::onPortValuePublished);
		return this;
	}
	
	public void activate(Button button) {
		log.info("Running action for button: {}", button);
		actionManager.run(button.getAction());
	}
	
	@SuppressWarnings("unchecked")
	private void onPortValuePublished(PortEvents.PortValuePublishedEvent<?> event) {
		if(event.value() instanceof ButtonValue)
			handleButtonEvent((DevicePort<ButtonValue>) event.port(), (ButtonValue) event.value());
	}
	
	private void handleButtonEvent(DevicePort<ButtonValue> port, ButtonValue value) {
		buttonSourceRepository.findBySourceAndValue(port.getPortId(), value)
				.subscribe(b -> activate(b.getButton()));
	}
}
