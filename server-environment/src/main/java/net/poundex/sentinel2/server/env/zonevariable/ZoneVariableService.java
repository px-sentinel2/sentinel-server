package net.poundex.sentinel2.server.env.zonevariable;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.PortEvents;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueWithMetadata;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableEvents.ZoneVariableUpdatedEvent;
import net.poundex.sentinel2.server.env.zonevariable.contributor.ZoneVariableContributor;
import net.poundex.sentinel2.server.env.zonevariable.contributor.ZoneVariableContributorRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
class ZoneVariableService implements ZoneVariableManager {
	
	private final PortEvents portEvents;
	private final ZoneVariableContributorRepository zoneVariableContributorRepository;
	private final ZoneVariableEvents zoneVariableEvents;

	private record ZoneZoneVariable<VT extends Value<VT>>(Zone zone, ZoneVariable<VT> zoneVariable) {}

	private final Map<ZoneZoneVariable<?>, Map<DevicePort<?>, ValueWithMetadata<?>>> contributedValues = new HashMap<>();
	private final Map<ZoneZoneVariable<?>, Value<?>> currentValues = new HashMap<>();
	
	@PostConstruct
	ZoneVariableService init() {
		portEvents.portValuePublished()
				.subscribe(this::onPortValuePublished);
		return this;
	}

	private void onPortValuePublished(PortEvents.PortValuePublishedEvent<? extends Value<?>> event) {
		zoneVariableContributorRepository
				.findByPortId(event.port().getPortId())
				.doOnNext(x -> contributePortValue(x, event))
				.subscribe();
	}

	@SuppressWarnings("unchecked")
	private <VT extends Value<VT>> void contributePortValue(ZoneVariableContributor zoneVariableContributor, PortEvents.PortValuePublishedEvent<? extends Value<?>> event) {
		ZoneVariable<VT> zoneVariable = (ZoneVariable<VT>) zoneVariableContributor.getZoneVariable();
		Zone zone = zoneVariableContributor.getZone();
		
		ZoneZoneVariable<VT> zoneZoneVariable = new ZoneZoneVariable<>(zone, zoneVariable);
		if( ! contributedValues.containsKey(zoneZoneVariable))
			contributedValues.put(zoneZoneVariable, new HashMap<>());

		Map<DevicePort<?>, ValueWithMetadata<?>> devicePortValueMap = contributedValues.get(zoneZoneVariable);
		devicePortValueMap.put(event.port(), new ValueWithMetadata<>((VT) event.value(), Instant.now()));
		
		VT value = zoneVariable.fold(devicePortValueMap.values().stream()
				.map(x -> (ValueWithMetadata<VT>) x)
				.collect(Collectors.toList()));
		currentValues.put(zoneZoneVariable, value);
		log.debug("Zone variable {} in zone {} new value is {} and fold is {}=>{}", 
				zoneZoneVariable.zoneVariable().getName(),
				zoneZoneVariable.zone().getName(),
				event.value(),
				devicePortValueMap.values(),
				value);
		zoneVariableEvents.zoneVariableUpdated(new ZoneVariableUpdatedEvent<>(zoneVariable, zone, value));
	}

	@Override
	@SuppressWarnings("unchecked")
	public <VT extends Value<VT>> Optional<VT> getValue(ZoneVariable<VT> zoneVariable, Zone zone) {
		return Optional.ofNullable((VT) currentValues.get(new ZoneZoneVariable<>(zone, zoneVariable)))
				.or(zoneVariable::getDefaultValue);
	}

	@Override
	public <VT extends Value<VT>> void setValue(ZoneVariable<VT> zoneVariable, Zone zone, VT value) {
		ZoneZoneVariable<VT> zoneZoneVariable = new ZoneZoneVariable<>(zone, zoneVariable);
		currentValues.put(zoneZoneVariable, value);
		log.debug("Zone variable {} in zone {} set value {}",
				zoneZoneVariable.zoneVariable().getName(),
				zoneZoneVariable.zone().getName(),
				value);
		zoneVariableEvents.zoneVariableUpdated(new ZoneVariableUpdatedEvent<>(zoneVariable, zone, value));
	}
}
