package net.poundex.sentinel2.server.env.zonevariable.trigger;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Repository
class InMemoryZoneVariableTriggerRepository extends AbstractReadOnlyRepository<ZoneVariableTrigger<?, ?>> implements ZoneVariableTriggerRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<ZoneVariableTrigger<?, ?>> objects = new LinkedList<>();

	@Override
	public <VT extends Value<VT>, T extends ZoneVariable<VT>> Mono<ZoneVariableTrigger<VT, T>> create(
			String name, Zone zone, T zoneVariable, VT value, Action action, EnvironmentPredicate environmentPredicate) {
		ZoneVariableTrigger<VT, T> o = ZoneVariableTrigger.<VT, T>builder()
				.name(name)
				.zone(zone)
				.zoneVariable(zoneVariable)
				.value(value)
				.action(action)
				.predicate(Optional.ofNullable(environmentPredicate))
				.build();
		objects.add(o);
		return Mono.just(o);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <VT extends Value<VT>, T extends ZoneVariable<VT>> Flux<ZoneVariableTrigger<VT, T>> findByZoneAndZoneVariable(
			Zone zone, T zoneVariable) {
		return Flux.fromIterable(objects)
				.filter(zvt -> zvt.getZone().equals(zone)
						&& zvt.getZoneVariable().equals(zoneVariable))
				.map(zvt -> (ZoneVariableTrigger<VT, T>) zvt);
	}
}
