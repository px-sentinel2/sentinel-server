package net.poundex.sentinel2.server.zwave.modem;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryZWaveModemRepository extends AbstractReadOnlyRepository<ZWaveModem> implements ZWaveModemRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<ZWaveModem> objects = new LinkedList<>();

	@Override
	public Mono<ZWaveModem> create(String name, String modemDevice) {
		ZWaveModem o = ZWaveModem.builder()
				.name(name)
				.modemDevice(modemDevice)
				.build();
		objects.add(o);
		return Mono.just(o);
	}
}
