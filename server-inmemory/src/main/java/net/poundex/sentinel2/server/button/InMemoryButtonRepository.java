package net.poundex.sentinel2.server.button;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryButtonRepository extends AbstractReadOnlyRepository<Button> implements ButtonRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<Button> objects = new LinkedList<>();
	
	@Override
	public Mono<Button> create(String name, Action action) {
		Button b = Button.builder()
				.name(name)
				.action(action)
				.build();
		objects.add(b);
		return Mono.just(b);
	}
}
