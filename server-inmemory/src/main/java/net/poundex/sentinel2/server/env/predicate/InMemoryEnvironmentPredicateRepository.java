package net.poundex.sentinel2.server.env.predicate;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryEnvironmentPredicateRepository extends AbstractReadOnlyRepository<EnvironmentPredicate> implements EnvironmentPredicateRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<EnvironmentPredicate> objects = new LinkedList<>();

	@Override
	public <VT extends Value<VT>> Mono<ZoneVariableValuePredicate<VT>> createZoneVariableValue(
			String name, Zone zone, ZoneVariable<VT> zoneVariable, VT value) {
		ZoneVariableValuePredicate<VT> ep = ZoneVariableValuePredicate.<VT>builder()
				.name(name)
				.zone(zone)
				.zoneVariable(zoneVariable)
				.value(value)
				.build();
		objects.add(ep);
		return Mono.just(ep);
	}
}
