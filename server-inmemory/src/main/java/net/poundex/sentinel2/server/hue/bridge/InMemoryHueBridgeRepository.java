package net.poundex.sentinel2.server.hue.bridge;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryHueBridgeRepository extends AbstractReadOnlyRepository<HueBridge> implements HueBridgeRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<HueBridge> objects = new LinkedList<>();

	@Override
	public Mono<HueBridge> create(String name, String address) {
		HueBridge o = HueBridge.builder()
				.name(name)
				.address(address)
				.build();
		objects.add(o);
		return Mono.just(o);
	}
}
