package net.poundex.sentinel2.server.zone;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Repository
class InMemoryZoneRepository extends AbstractReadOnlyRepository<Zone> implements ZoneRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<Zone> objects = new LinkedList<>();

	@Override
	public Mono<Zone> create(String name, Zone parent) {
		Zone o = Zone.builder()
				.name(name)
				.parent(Optional.ofNullable(parent))
				.build();
		objects.add(o);
		return Mono.just(o);
	}
}
