package net.poundex.sentinel2.server.button;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.env.value.ButtonValue;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryButtonSourceSourceRepository extends AbstractReadOnlyRepository<ButtonSource> implements ButtonSourceRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<ButtonSource> objects = new LinkedList<>();
	
	@Override
	public Mono<ButtonSource> create(Button button, URI source, ButtonValue value) {
		ButtonSource b = ButtonSource.builder()
				.button(button)
				.source(source)
				.value(value)
				.build();
		objects.add(b);
		return Mono.just(b);
	}
	
	@Override
	public Mono<ButtonSource> findBySourceAndValue(URI source, ButtonValue buttonValue) {
		return Flux.fromIterable(objects)
				.filter(bs -> bs.getSource().equals(source)
						&& bs.getValue().equals(buttonValue))
				.singleOrEmpty();
	}
}
