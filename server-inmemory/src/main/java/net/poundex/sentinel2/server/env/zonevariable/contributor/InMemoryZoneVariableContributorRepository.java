package net.poundex.sentinel2.server.env.zonevariable.contributor;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryZoneVariableContributorRepository extends AbstractReadOnlyRepository<ZoneVariableContributor> implements ZoneVariableContributorRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<ZoneVariableContributor> objects = new LinkedList<>();

	@Override
	public Mono<ZoneVariableContributor> create(ZoneVariable<?> zoneVariable, Zone zone, URI portId) {
		ZoneVariableContributor o = ZoneVariableContributor.builder()
				.zoneVariable(zoneVariable)
				.zone(zone)
				.portId(portId)
				.build();
		objects.add(o);
		return Mono.just(o);
	}

	@Override
	public Flux<ZoneVariableContributor> findByPortId(URI portId) {
		return Flux.fromIterable(objects)
				.filter(zvc -> zvc.getPortId().equals(portId));
	}
}
