package net.poundex.sentinel2.server.nest.thermostat;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryNestThermostatRepository extends AbstractReadOnlyRepository<NestThermostat> implements NestThermostatRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<NestThermostat> objects = new LinkedList<>();
	
	@Override
	public Mono<NestThermostat> create(String name) {
		NestThermostat o = NestThermostat.builder()
				.name(name)
				.build();
		objects.add(o);
		return Mono.just(o);
	}
}
