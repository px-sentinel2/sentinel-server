package net.poundex.sentinel2.server.env.scene;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.env.value.ColourValue;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Repository
class InMemorySceneRepository extends AbstractReadOnlyRepository<Scene> implements SceneRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<Scene> objects = new LinkedList<>();

	@Override
	public Mono<Scene> create(String name, ColourValue... colourValues) {
		Scene o = Scene.builder()
				.name(name)
				.values(Arrays.asList(colourValues))
				.build();
		objects.add(o);
		return Mono.just(o);
	}
}
