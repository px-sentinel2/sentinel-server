package net.poundex.sentinel2.server.env.zonevariable;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.env.value.SceneValue;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Repository
class InMemoryZoneVariableRepository extends AbstractReadOnlyRepository<ZoneVariable<?>> implements ZoneVariableRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<ZoneVariable<?>> objects = new LinkedList<>();
	
	@SuppressWarnings("unchecked")
	@Override
	public <T extends ZoneVariable<?>> Mono<T> findByName(String name) {
		return Flux.fromIterable(objects)
				.filter(zv -> zv.getName().equals(name))
				.map(zv -> (T) zv)
				.singleOrEmpty();
	}

	@Override
	public Mono<NumericZoneVariable> createNumeric(
			String name, NumericZoneVariable.NumericFoldStrategy foldStrategy, NumericValue defaultValue) {
		NumericZoneVariable zv = NumericZoneVariable.builder()
				.name(name)
				.zoneFoldStrategy(foldStrategy)
				.defaultValue(Optional.ofNullable(defaultValue))
				.build();
		objects.add(zv);
		return Mono.just(zv);
	}

	@Override
	public Mono<BooleanZoneVariable> createBool(
			String name, BooleanZoneVariable.BooleanFoldStrategy foldStrategy, BooleanValue defaultValue) {
		BooleanZoneVariable zv = BooleanZoneVariable.builder()
				.name(name)
				.zoneFoldStrategy(foldStrategy)
				.defaultValue(Optional.ofNullable(defaultValue))
				.build();
		objects.add(zv);
		return Mono.just(zv);
	}

	@Override
	public Mono<SceneZoneVariable> createScene(String name, SceneValue defaultValue) {
		SceneZoneVariable zv = SceneZoneVariable.builder()
				.name(name)
				.defaultValue(Optional.ofNullable(defaultValue))
				.build();
		objects.add(zv);
		return Mono.just(zv);
	}
}
