package net.poundex.sentinel2.server.env.action;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.appliance.Appliance;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.scene.Scene;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

@Repository
class InMemoryActionRepository extends AbstractReadOnlyRepository<Action> implements ActionRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<Action> objects = new LinkedList<>();


	@Override
	public <VT extends Value<VT>> Mono<WritePortValueAction<VT>> createWritePortValueAction(
			String name, Appliance appliance, URI portPath, VT value) {
		WritePortValueAction<VT> a = WritePortValueAction.<VT>builder()
				.name(name)
				.appliance(appliance)
				.portPath(portPath)
				.value(value)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public Mono<MacroAction> createMacroAction(String name, List<? extends Action> actions) {
		MacroAction a = MacroAction.builder()
				.name(name)
				.actions(actions)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public Mono<SetZoneToSceneAction> createSetZoneToScene(String name, Zone zone, Scene scene) {
		SetZoneToSceneAction a = SetZoneToSceneAction.builder()
				.name(name)
				.zone(zone)
				.scene(scene)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public Mono<SetSceneActivationAction> createSetSceneActivation(
			String name, Zone zone, SetSceneActivationAction.Activation activation) {
		SetSceneActivationAction a = SetSceneActivationAction.builder()
				.name(name)
				.zone(zone)
				.activation(activation)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public <VT extends Value<VT>> Mono<SetZoneVariableAction<VT>> createSetZoneVariable(
			String name, Zone zone, ZoneVariable<VT> zoneVariable, VT value) {
		SetZoneVariableAction<VT> a = SetZoneVariableAction.<VT>builder()
				.name(name)
				.zone(zone)
				.zoneVariable(zoneVariable)
				.value(value)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public Mono<RepublishZoneVariableAction> createRepublishZoneVariable(String name, Zone zone, ZoneVariable<?> zoneVariable) {
		RepublishZoneVariableAction a = RepublishZoneVariableAction.builder()
				.name(name)
				.zone(zone)
				.zoneVariable(zoneVariable)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public Mono<ConditionalAction> createConditional(String name, EnvironmentPredicate predicate, Action action) {
		ConditionalAction a = ConditionalAction.builder()
				.name(name)
				.predicate(predicate)
				.action(action)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public Mono<RunFirstConditionalActionAction> createRunFirstConditionalAction(String name, List<ConditionalAction> actions) {
		RunFirstConditionalActionAction a = RunFirstConditionalActionAction.builder()
				.name(name)
				.actions(actions)
				.build();
		objects.add(a);
		return Mono.just(a);
	}

	@Override
	public <VT extends Value<VT>> Mono<CopyZoneVariableAction<VT>> createCopyZoneVariableAction(
			String name, Zone sourceZone, ZoneVariable<VT> source, Zone destinationZone, ZoneVariable<VT> destination) {
		CopyZoneVariableAction<VT> a = CopyZoneVariableAction.<VT>builder()
				.name(name)
				.sourceZone(sourceZone)
				.source(source)
				.destinationZone(destinationZone)
				.destination(destination)
				.build();
		objects.add(a);
		return Mono.just(a);
	}
}
