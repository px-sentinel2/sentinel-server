package net.poundex.sentinel2.server.appliance;

import lombok.AccessLevel;
import lombok.Getter;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.inmemory.AbstractReadOnlyRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.*;

@Repository
class InMemoryApplianceRepository extends AbstractReadOnlyRepository<Appliance> implements ApplianceRepository {

	@Getter(AccessLevel.PROTECTED)
	private final List<Appliance> objects = new LinkedList<>();

	@Override
	public Mono<Appliance> create(String name, Zone zone, URI deviceId, ApplianceRole... roles) {
		Appliance appliance = Appliance.builder()
				.name(name)
				.zone(zone)
				.deviceId(deviceId)
				.roles(new HashSet<>(Arrays.asList(roles)))
				.build();
		objects.add(appliance);
		return Mono.just(appliance);
	}

	@Override
	public Flux<Appliance> findAllByZoneWithRoles(Zone zone, Set<ApplianceRole> roles) {
		return Flux.fromIterable(objects)
				.filter(a -> a.getZone().equals(zone)
						&& a.getRoles().containsAll(roles));
	}
}
