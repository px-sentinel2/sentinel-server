package net.poundex.sentinel2.server.inmemory;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.SentinelObject;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public abstract class AbstractReadOnlyRepository<T extends SentinelObject> implements ReadOnlyRepository<T> {
	
	abstract protected Iterable<T> getObjects();

	@Override
	public Flux<T> findAll() {
		return Flux.fromIterable(getObjects());
	}

	@Override
	public Mono<T> findById(String id) {
		return Flux.fromIterable(getObjects())
				.filter(a -> a.getId().equals(id))
				.singleOrEmpty();
	}

	@Override
	public Flux<T> findAllById(Iterable<String> ids) {
		Set<String> idSet = StreamSupport.stream(ids.spliterator(), false)
				.collect(Collectors.toSet());
		return Flux.fromIterable(getObjects())
				.filter(a -> idSet.contains(a.getId()));
	}
}
