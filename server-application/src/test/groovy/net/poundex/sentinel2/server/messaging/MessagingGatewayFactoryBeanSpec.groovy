package net.poundex.sentinel2.server.messaging

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.integration.channel.PublishSubscribeChannel
import org.springframework.messaging.Message
import spock.lang.Specification

import java.util.concurrent.Executor

class MessagingGatewayFactoryBeanSpec extends Specification {
	interface SomeEventBus {
		void someEvent(Object e)
		Subscribable<Object> someEvent()
	}

	Executor executor = Stub()
	MessagingGatewayFactoryBean<SomeEventBus> factoryBean =
			new MessagingGatewayFactoryBean<>(SomeEventBus)

	ConfigurableListableBeanFactory beanFactory = Stub(ConfigurableListableBeanFactory) {
		getBean("eventDispatchPool", _) >> executor
	}
	ApplicationContext applicationContext = Stub(ApplicationContext)

	Object someEvent = new Object()

	void setup() {
		factoryBean.applicationContext = applicationContext
		factoryBean.beanFactory = beanFactory
	}

	void "Registers message channels with application context"() {
		given:
		ConfigurableListableBeanFactory beanFactory2 = Mock()
		applicationContext.getAutowireCapableBeanFactory() >> beanFactory2

		when:
		factoryBean.object

		then:
		1 * beanFactory2.registerSingleton("someEventChannel", _ as PublishSubscribeChannel)
	}

	void "Sends events"() {
		given:
		PublishSubscribeChannel channel = Mock()
		applicationContext.getAutowireCapableBeanFactory() >> beanFactory
		beanFactory.getBean("someEventChannel", _) >> channel
		SomeEventBus eventBus = factoryBean.object

		when:
		eventBus.someEvent(someEvent)

		then:
		1 * channel.send({ Message m -> m.payload.is someEvent })
	}

	void "Publishes events"() {
		given:
		PublishSubscribeChannel channel = Mock()
		applicationContext.getAutowireCapableBeanFactory() >> beanFactory
		beanFactory.getBean("someEventChannel", _) >> channel
		factoryBean.channelFactory = { channel }
		SomeEventBus eventBus = factoryBean.object

		when:
		eventBus.someEvent().subscribe({})

		then:
		1 * channel.subscribe(_)
	}

	void "Doesn't break Object"() {
		given:
		applicationContext.getAutowireCapableBeanFactory() >> beanFactory
		SomeEventBus eventBus = factoryBean.object

		expect:
		eventBus == eventBus
		eventBus.toString() instanceof String
		eventBus.hashCode() instanceof Integer
	}
}
