package net.poundex.sentinel2.server.graphql

import graphql.GraphQLError
import graphql.schema.DataFetchingEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.exception.NotFoundException
import net.poundex.sentinel2.test.PublisherUtils
import org.springframework.graphql.execution.ErrorType
import spock.lang.Specification
import spock.lang.Subject

import javax.validation.ConstraintViolation
import javax.validation.ConstraintViolationException
import javax.validation.Path

class ExceptionResolverSpec extends Specification implements PublisherUtils {
	
	DataFetchingEnvironment env = Stub()
	
	@Subject
	ExceptionResolver exceptionResolver = new ExceptionResolver()
	
	void "Resolve NotFoundException"() {
		when:
		List<GraphQLError> errors = block exceptionResolver.resolveException(new NotFoundException(Zone.class, "123"), env)
		
		then:
		errors.size() == 1
		with(errors.first()) {
			errorType == ErrorType.NOT_FOUND
		}
	}
	
	void "Handle field errors"() {
		given:
		ConstraintViolation cv1 = Stub() {
			getPropertyPath() >> Stub(Path) {
				toString() >> "foo.bar"
			}
		}
		ConstraintViolation cv2 = Stub() {
			getPropertyPath() >> Stub(Path) {
				toString() >> "foo.baz"
			}
		}

		when:
		List<GraphQLError> errors = block exceptionResolver
				.resolveException(new ConstraintViolationException(
						Set.of(cv1, cv2)), env)

		then:
		errors.size() == 1
		with(errors.first()) {
			errorType == ErrorType.BAD_REQUEST
			extensions.error == "UNPROCESSABLE_ENTITY"
			with((List<Map<String, Object>>) extensions.validationErrors) {
				size() == 2
				it.find {
					it.type == "FIELD_ERROR" &&
							it.path == "foo.bar"
				}
				it.find {
					it.type == "FIELD_ERROR" &&
							it.path == "foo.baz"
				}
			}
		}
	}
}
