package net.poundex.sentinel2.server.graphql

import graphql.language.StringValue
import graphql.schema.CoercingSerializeException
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

class InstantScalarCoercingSpec extends Specification {
	
	private static final Instant now = Instant.now()
	
	@Subject
	InstantScalarCoercing coercing = new InstantScalarCoercing()
	
	void "Serialize"() {
		expect:
		coercing.serialize(now) == now.toString() 
		
		when:
		coercing.serialize("")
		
		then:
		thrown(CoercingSerializeException)
	}
	
	void "Parse value"() {
		expect:
		coercing.parseValue(now) == now
		coercing.parseValue(now.toString()) == now
		
		when:
		coercing.parseValue(123)
		
		then:
		thrown(CoercingSerializeException)
	}
	
	void "Parse literal"() {
		expect:
		coercing.parseLiteral(new StringValue(now.toString())) == now
		
		when:
		coercing.parseLiteral(new Object())

		then:
		thrown(CoercingSerializeException)
	}


	void "Value to literal"() {
		expect:
		coercing.valueToLiteral(now).value == now.toString()

		when:
		coercing.valueToLiteral(new Object())

		then:
		thrown(CoercingSerializeException)
	}


}
