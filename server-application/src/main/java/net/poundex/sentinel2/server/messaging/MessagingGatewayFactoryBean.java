package net.poundex.sentinel2.server.messaging;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.util.IntegrationReactiveUtils;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.support.MessageBuilder;
import reactor.core.publisher.Flux;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
class MessagingGatewayFactoryBean<T> implements FactoryBean<T>, BeanFactoryAware, ApplicationContextAware {

	private record PubSubMethodPair(Method publish, Method subscribe) {
		String getMethodName() {
			return publish().getName();
		}
	}
	private record PubSubConfig<P>(PubSubMethodPair methodPair, Flux<Message<P>> flux) { }
	
	private final Class<T> gatewayClass;
	private final GatewaySupport<T> support;
	
	private BeanFactory beanFactory;
	private ApplicationContext applicationContext;
	private Map<String, PubSubConfig<?>> pubSubConfigs;
	private Function<PubSubMethodPair, SubscribableChannel> channelFactory = 
			this::defaultChannelFactory;
	
	public MessagingGatewayFactoryBean(Class<T> gatewayClass) {
		this.gatewayClass = gatewayClass;
		this.support = new GatewaySupport<>(gatewayClass);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getObject() {
		
		initPubSub();

		return (T) Proxy.newProxyInstance(
				getClass().getClassLoader(),
				new Class[]{ gatewayClass },
				this::handleInvocation);
	}

	private Object handleInvocation(Object proxy, Method method, Object[] args) throws Exception {
		if ( ! pubSubConfigs.containsKey(method.getName()))
			return method.invoke(support, args);

		PubSubConfig<?> pubSubConfig = pubSubConfigs.get(method.getName());

		if (method.equals(pubSubConfig.methodPair().publish()))
			return beanFactory.getBean(method.getName() + "Channel", MessageChannel.class)
					.send(MessageBuilder.withPayload(args[0]).build());
		else
			return (Subscribable<?>) consumer -> pubSubConfig.flux()
					.map(Message::getPayload)
					.doOnNext(consumer)
					.onErrorContinue((t, o) -> log.error("Error dispatching event " + o, t))
					.subscribe();
	}


	private void initPubSub() {
		pubSubConfigs = Arrays.stream(gatewayClass.getMethods())
				.map(Method::getName)
				.distinct()
				.map(this::findMethodsNamed)
				.filter(this::isPubSubMethodPair)
				.map(lm -> new PubSubMethodPair(
						lm.stream().filter(m -> m.getReturnType().equals(void.class)).findFirst().get(),
						lm.stream().filter(m -> m.getReturnType().equals(Subscribable.class)).findFirst().get()))
				.map(this::configureChannel)
				.collect(Collectors.toMap(kv -> kv.methodPair().getMethodName(), kv -> kv));
	}

	private List<Method> findMethodsNamed(String methodName) {
		return Arrays.stream(gatewayClass.getMethods())
				.filter(m -> m.getName().equals(methodName))
				.toList();
	}

	private boolean isPubSubMethodPair(List<Method> lm) {
		return lm.size() == 2
				&& lm.stream()
				.map(Method::getReturnType)
				.collect(Collectors.toSet())
				.containsAll(Set.of(void.class, Subscribable.class));
	}

	private PubSubConfig<Object> configureChannel(PubSubMethodPair mp) {
		SubscribableChannel channel = channelFactory.apply(mp);

		((ConfigurableBeanFactory) applicationContext.getAutowireCapableBeanFactory())
				.registerSingleton(mp.getMethodName() + "Channel", channel);
		
		return new PubSubConfig<>(mp, IntegrationReactiveUtils.messageChannelToFlux(channel));
	}

	private PublishSubscribeChannel defaultChannelFactory(PubSubMethodPair pubSubMethodPair) {
		PublishSubscribeChannel channel = new PublishSubscribeChannel(
				beanFactory.getBean("eventDispatchPool", Executor.class));
		channel.setComponentName(pubSubMethodPair.getMethodName());
		return channel;
	}

	void setChannelFactory(Function<PubSubMethodPair, SubscribableChannel> channelFactory) {
		this.channelFactory = channelFactory;
	}

	@Override
	public Class<?> getObjectType() {
		return gatewayClass;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@RequiredArgsConstructor
	@Data
	private static class GatewaySupport<T> {
		private final Class<T> gatewayClass;

		@Override
		public String toString() {
			return "Messaging Gateway for " + gatewayClass.getSimpleName();
		}
	}
}
