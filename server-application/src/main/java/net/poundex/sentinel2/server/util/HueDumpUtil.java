package net.poundex.sentinel2.server.util;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.appliance.Appliance;
import net.poundex.sentinel2.server.appliance.ApplianceRepository;
import net.poundex.sentinel2.server.device.DeviceManager;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.ReadableDevicePort;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@Profile("huedump")
@RequiredArgsConstructor
@Slf4j
public class HueDumpUtil implements ApplicationListener<ApplicationReadyEvent> {

	private final DeviceManager deviceManager;
	private final ApplianceRepository applianceRepository;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		new Thread(() -> {
			Try.run(() -> Thread.sleep(3500)).get();
			
			applianceRepository.findAll().collectMultimap(Appliance::getZone, kv -> kv).block()
					.forEach((key, value) -> log.info("\n{}\n===============\n{}\n", key.getName(),
							value.stream().map(a -> String.format("%-15s: %s",  a.getName(),
											deviceManager.getDevice(a.getDeviceId())
													.flatMap(d -> d.getPort(DevicePort.COLOUR))
													.map(dp -> ((ReadableDevicePort) dp))
													.map(ReadableDevicePort::readValue)
													.orElse(null)))
									.collect(Collectors.joining("\n"))));
			
			event.getApplicationContext().close();
			System.exit(0);

		}).start();
	}
}
