package net.poundex.sentinel2.server.graphql;

import graphql.GraphQLError;
import graphql.GraphqlErrorBuilder;
import graphql.schema.DataFetchingEnvironment;
import net.poundex.sentinel2.server.exception.NotFoundException;
import org.springframework.graphql.execution.DataFetcherExceptionResolver;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Map;

@Component
class ExceptionResolver implements DataFetcherExceptionResolver {

	@Override
	public Mono<List<GraphQLError>> resolveException(Throwable exception, DataFetchingEnvironment environment) {
		GraphqlErrorBuilder<?> builder = GraphqlErrorBuilder.newError(environment);
		
		if (exception instanceof NotFoundException nfex)
			return handleNotFound(nfex, builder);
		if(exception instanceof ConstraintViolationException cvex)
			return handleValidationFailure(cvex, builder);

		return Mono.empty();
	}

	private static Mono<List<GraphQLError>> handleNotFound(NotFoundException nfex, GraphqlErrorBuilder<?> builder) {
		return Mono.just(List.of(builder
				.errorType(ErrorType.NOT_FOUND)
				.message(nfex.getMessage())
				.build()));
	}

	private Mono<List<GraphQLError>> handleValidationFailure(ConstraintViolationException cvex, GraphqlErrorBuilder<?> builder) {
		return Mono.just(List.of(builder
				.errorType(ErrorType.BAD_REQUEST)
				.message("There are validation failures")
				.extensions(Map.of(
						"error", "UNPROCESSABLE_ENTITY",
						"validationErrors", cvex.getConstraintViolations().stream()
								.map(cv -> Map.of(
										"type", "FIELD_ERROR",
										"path", cv.getPropertyPath().toString()))
								.toList()))
				.build()));
	}
}
