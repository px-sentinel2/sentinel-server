package net.poundex.sentinel2.server.graphql;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;

import java.time.Instant;

class InstantScalarCoercing implements Coercing<Instant, String> {
	@Override
	public String serialize(Object dataFetcherResult) throws CoercingSerializeException {
		if(dataFetcherResult instanceof Instant instant)
			return instant.toString();
		
		throw new CoercingSerializeException(String.format(
				"Don't know how to deal with %s", dataFetcherResult.getClass()));
	}

	@Override
	public Instant parseValue(Object input) throws CoercingParseValueException {
		if(input instanceof Instant instant)
			return instant;
		if(input instanceof CharSequence string)
			return Instant.parse(string);

		throw new CoercingSerializeException(String.format(
				"Don't know how to deal with %s", input.getClass()));
	}

	@Override
	public Instant parseLiteral(Object input) throws CoercingParseLiteralException {
		if(input instanceof StringValue sv)
			return Instant.parse(sv.getValue());

		throw new CoercingSerializeException(String.format(
				"Don't know how to deal with %s", input.getClass()));
	}

	@Override
	public StringValue valueToLiteral(Object input) {
		return new StringValue(serialize(input));
	}
}
