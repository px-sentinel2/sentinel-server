package net.poundex.sentinel2.server.messaging;

import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.device.DriverEvents;
import net.poundex.sentinel2.server.device.port.PortEvents;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableEvents;
import net.poundex.sentinel2.server.zwave.controller.ZWaveModemEvents;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

@Configuration
@Slf4j
class MessagingConfiguration {
	
	@Bean
	public FactoryBean<DriverEvents> driverEventsGateway() {
		return new MessagingGatewayFactoryBean<>(DriverEvents.class);
	}
	
	@Bean
	public FactoryBean<PortEvents> portEventsGateway() {
		return new MessagingGatewayFactoryBean<>(PortEvents.class);
	}

	@Bean
	public FactoryBean<ZoneVariableEvents> zoneVariableEventsGateway() {
		return new MessagingGatewayFactoryBean<>(ZoneVariableEvents.class);
	}
	
	@Bean
	public FactoryBean<ZWaveModemEvents> zWaveModemEventsGateway() {
		return new MessagingGatewayFactoryBean<>(ZWaveModemEvents.class);
	}
	
	@Bean
	public ExecutorService eventDispatchPool() {
		return Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new ThreadFactory() {
			
			private int tc = 0;
			
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r);
				t.setDaemon(true);
				t.setName("sentinel-event-" + tc++);
				t.setUncaughtExceptionHandler((t1, e) -> log.error("Error dispatching event", e));
				return t;
			}
		});
	}
}
