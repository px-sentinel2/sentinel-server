package net.poundex.sentinel2.server.graphql;

import graphql.schema.GraphQLScalarType;
import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.SentinelObject;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.GenericTypeResolver;
import org.springframework.graphql.execution.BatchLoaderRegistry;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

import java.lang.reflect.ParameterizedType;

//@Configuration
@RequiredArgsConstructor
class GraphQlConfiguration implements ApplicationListener<ContextRefreshedEvent> {
	
	private final BatchLoaderRegistry batchLoaderRegistry;
	
	@Bean
	public RuntimeWiringConfigurer runtimeWiringConfigurer() {
		GraphQLScalarType scalarType = new GraphQLScalarType.Builder()
				.name("Instant").coercing(new InstantScalarCoercing()).build();
		
		return wiringBuilder -> wiringBuilder.scalar(scalarType);
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		event.getApplicationContext()
				.getBeansOfType(ReadOnlyRepository.class)
				.values()
				.forEach(this::registerLoader);
	}
	
	private <T extends SentinelObject> void registerLoader(ReadOnlyRepository<T> repo) {
		batchLoaderRegistry
				.forTypePair(String.class, getRepositoryType(repo))
				.registerMappedBatchLoader((ids, env) -> repo
						.findAllById(ids)
						.collectMap(SentinelObject::getId, so -> so));
	}

	@SuppressWarnings("unchecked")
	private <T extends SentinelObject> Class<T> getRepositoryType(ReadOnlyRepository<T> repository) {
		return (Class<T>) GenericTypeResolver.getTypeVariableMap(repository.getClass())
				.values()
				.stream()
				.map(t -> {
					if(t instanceof ParameterizedType pt)
						return pt.getRawType();
					return t;
				})
				.findFirst()
				.orElseThrow(IllegalStateException::new);
	}
}
