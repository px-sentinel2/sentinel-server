package net.poundex.sentinel2.server;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.server.appliance.ApplianceRepository;
import net.poundex.sentinel2.server.device.DeviceManager;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableManager;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableRepository;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Getter
@Component
class DefaultSentinelEnvironment implements SentinelEnvironment {
	private final DeviceManager deviceManager;
	private final ApplianceRepository applianceRepository;
	private final ZoneVariableManager zoneVariableManager;
	private final ZoneVariableRepository zoneVariableRepository;
}
