package net.poundex.sentinel2.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SentinelServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(SentinelServerApplication.class, args);
	}
}
