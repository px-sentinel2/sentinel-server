package net.poundex.sentinel2.server.test

import java.util.regex.Pattern

trait MongoUtils {
	static final Pattern anObjectId = ~/[a-f0-9]{24}/
}