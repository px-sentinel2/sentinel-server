package net.poundex.sentinel2.server.api


import net.poundex.sentinel2.test.BatchLoadingUtils
import net.poundex.sentinel2.test.PublisherUtils
import org.dataloader.DataLoader
import spock.lang.Specification
import spock.lang.Subject

import java.util.concurrent.CompletableFuture
import java.util.function.Function

class BatchLoadingHelperSpec extends Specification implements BatchLoadingUtils, PublisherUtils {

	private static final someModel = new Object()
	private static final someDomainObject = new Object()
	
	DataLoader<String, Object> dataLoader = Stub()
	
	@Subject
	BatchLoadingHelper batchLoadingHelper = new BatchLoadingHelper()
	
	void "Return else case when field set contains only ID"() {
		when:
		Object r = block batchLoadingHelper.loadIfNecessary("1", justId(), dataLoader)
				.andMap({ null })
				.orElse({ -> someModel })
		
		then:
		r.is someModel
	}
	

	void "Load data and return mapped result when other fields present"() {
		given:
		dataLoader.load("1") >> CompletableFuture.completedFuture(someDomainObject)
		Function<Object, Object> mapper = Mock()
		
		when:
		Object r = block batchLoadingHelper.loadIfNecessary("1", fields("name"), dataLoader)
				.andMap(mapper)
				.orElse({ null })

		then:
		r.is someModel
		1 * mapper.apply(someDomainObject) >> someModel
	}
}
