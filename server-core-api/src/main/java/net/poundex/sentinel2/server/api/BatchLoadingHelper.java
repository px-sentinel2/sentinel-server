package net.poundex.sentinel2.server.api;

import graphql.schema.DataFetchingFieldSelectionSet;
import lombok.RequiredArgsConstructor;
import org.dataloader.DataLoader;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

@Component
public class BatchLoadingHelper {
	public <T, R> AndMap<T, R> loadIfNecessary(String id, DataFetchingFieldSelectionSet fieldSelectionSet, DataLoader<String, T> loader) {
		return new AndMap<>(id, fieldSelectionSet, loader);
	}

	@RequiredArgsConstructor
	public static class AndMap<T, R> {
		
		private final String id;
		private final DataFetchingFieldSelectionSet fieldSelectionSet;
		private final DataLoader<String, T> loader;
		
		public OrElse andMap(Function<T, R> mapper) {
			return new OrElse(mapper);
		}
		
		public Optional<Mono<T>> get() {
			return doGet();
		}
		
		private Optional<Mono<T>> doGet() {
			if (fieldSetContainsAnythingOtherThanId())
				return Optional.of(Mono.fromCompletionStage(loader.load(id)));
			
			return Optional.empty();
		}
		
		private boolean fieldSetContainsAnythingOtherThanId() {
			return fieldSelectionSet.getFields().stream().anyMatch(f -> ! f.getName().equals("id"));
		}

		@RequiredArgsConstructor
		public class OrElse {
			private final Function<T, R> mapper;
			
			@SuppressWarnings("unchecked")
			public <RR extends R> Mono<RR> orElse(Supplier<RR> supplier) {
				return (Mono<RR>) doGet().map(m -> m.map(mapper))
						.orElseGet(() -> Mono.fromSupplier(supplier));
			}
		}
	}

}
