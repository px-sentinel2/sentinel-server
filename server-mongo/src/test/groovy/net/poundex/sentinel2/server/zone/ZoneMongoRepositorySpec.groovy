package net.poundex.sentinel2.server.zone

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class ZoneMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {

	private static final MongoZone MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoZone MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)
	
	private static final MongoZone MONGO_OBJ_2 = aMongoObject(2, true, MONGO_OBJ_1)
	private static final MongoZone MONGO_OBJ_2_UNSAVED = aMongoObject(2, false, MONGO_OBJ_1)
	
	private static final Zone MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final Zone MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2, MAPPED_OBJ_1)
	
	ZoneMongoDao dao = Stub(ZoneMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	ZoneMongoRepository repository = new ZoneMongoRepository(dao, mappers)

	void "Create new"() {
		expect:
		block(repository.create(MONGO_OBJ_1.name, null)) == MAPPED_OBJ_1
		block(repository.create(MONGO_OBJ_2.name, MAPPED_OBJ_1)) == MAPPED_OBJ_2
	}
	
	private static MongoZone aMongoObject(def n, boolean saved, MongoZone parentZone = null) { 
		return MongoZone.builder().with {
			if(saved) id(new ObjectId())
			name("name-${n}")
			parent(parentZone)
			build()
		}
	}
	
	private static Zone aSentinelObject(MongoZone zone, Zone parentZone = null) {
		return Zone.builder().with {
			id(zone.id.toString())
			name(zone.name)
			parent(Optional.ofNullable(parentZone))
			build()
		}
	}

}
