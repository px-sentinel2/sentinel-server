package net.poundex.sentinel2.server.button


import net.poundex.sentinel2.server.env.value.ButtonValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class ButtonSourceMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {

	static final URI SOURCE = "dev://dev/1".toURI()
	static final ButtonValue BUTTON_VALUE = new ButtonValue(1, ButtonValue.Type.PUSHED)
	static final Button BUTTON = Button.builder().build()
	static final MongoButton MONGO_BUTTON = MongoButton.builder().build()

	private static final MongoButtonSource MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoButtonSource MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final MongoButtonSource MONGO_OBJ_2 = aMongoObject(2, true)
	private static final MongoButtonSource MONGO_OBJ_2_UNSAVED = aMongoObject(2, false)

	private static final ButtonSource MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final ButtonSource MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)

	ButtonSourceMongoDao dao = Stub(ButtonSourceMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		
		findBySourceAndRawValue(SOURCE, "rv") >> Mono.just(MONGO_OBJ_1)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		from(BUTTON) >> MONGO_BUTTON
		to(MONGO_BUTTON) >> BUTTON
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		writeRaw(BUTTON_VALUE) >> "rv"
		readRaw("rv") >> BUTTON_VALUE
	}
	
	@Subject
	ButtonSourceMongoRepository repository = new ButtonSourceMongoRepository(dao, mappers, valueConverter)

	void "Create new"() {
		expect:
		block(repository.create(MAPPED_OBJ_1.button, MAPPED_OBJ_1.source, MAPPED_OBJ_1.value)) == MAPPED_OBJ_1
	}
	
	void "Finds by source and value"() {
		expect:
		block(repository.findBySourceAndValue(SOURCE, BUTTON_VALUE)) == MAPPED_OBJ_1
	}
	
	private static MongoButtonSource aMongoObject(def n, boolean saved) {
		return MongoButtonSource.builder().with {
			if(saved) id(new ObjectId())
			button(MONGO_BUTTON)
			source(SOURCE)
			rawValue("rv")
			build()
		}
	}

	private static ButtonSource aSentinelObject(MongoButtonSource obj) {
		return ButtonSource.builder().with {
			id(obj.id.toString())
			button(BUTTON)
			source(SOURCE)
			value(BUTTON_VALUE)
			build()
		}
	}
}
