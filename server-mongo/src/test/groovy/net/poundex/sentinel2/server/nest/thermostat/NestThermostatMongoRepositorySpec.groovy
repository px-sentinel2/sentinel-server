package net.poundex.sentinel2.server.nest.thermostat

import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class NestThermostatMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {

	private static final MongoNestThermostat MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoNestThermostat MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final MongoNestThermostat MONGO_OBJ_2 = aMongoObject(2, true)
	private static final MongoNestThermostat MONGO_OBJ_2_UNSAVED = aMongoObject(2, false)

	private static final NestThermostat MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final NestThermostat MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)

	NestThermostatMongoDao dao = Stub(NestThermostatMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		_ >> { unexpectedCall(delegate) }
	}
	
	NestCloudService nestCloudService = Stub() {
		getServiceUrl(MONGO_OBJ_1.name) >> Optional.of(MONGO_OBJ_1.serviceUrl)
	}

	@Subject
	NestThermostatRepository repository = new NestThermostatMongoRepository(dao, mappers, nestCloudService)
	
	void "Create new"() {
		given:
		dao.save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)

		expect:
		block(repository.create(MONGO_OBJ_1.name)) == MAPPED_OBJ_1
	}

	private static MongoNestThermostat aMongoObject(def n, boolean saved) {
		return MongoNestThermostat.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			serviceUrl("service-url-${n}")
			build()
		}
	}

	private static NestThermostat aSentinelObject(MongoNestThermostat obj) {
		return NestThermostat.builder().with {
			id(obj.id.toString())
			name(obj.name)
			serviceUrl(obj.serviceUrl)
			build()
		}
	}
}
