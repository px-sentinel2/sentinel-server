package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.appliance.Appliance
import net.poundex.sentinel2.server.appliance.MongoAppliance
import net.poundex.sentinel2.server.env.predicate.AbstractMongoEnvironmentPredicate
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate
import net.poundex.sentinel2.server.env.scene.MongoScene
import net.poundex.sentinel2.server.env.scene.Scene
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class ActionMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {

	private static final Appliance AN_APPLIANCE = Appliance.builder().name("app-1").build()
	private static final MongoAppliance A_MONGO_APPLIANCE = MongoAppliance.builder().name(AN_APPLIANCE.name).build()

	private static final Value A_VALUE = new Value() {}
	
	private static final Zone ZONE_1 = Zone.builder().build()
	private static final Zone ZONE_2 = Zone.builder().build()
	private static final Scene SCENE = Scene.builder().build()
	private static final MongoZone MONGO_ZONE_1 = MongoZone.builder().build()
	private static final MongoZone MONGO_ZONE_2 = MongoZone.builder().build()
	private static final MongoScene MONGO_SCENE = MongoScene.builder().build()

	private static final MongoWritePortValueAction MONGO_WPV_1 = aMongoWritePortValue(1, true)
	private static final MongoWritePortValueAction MONGO_WPV_1_UNSAVED = aMongoWritePortValue(1, false)
	private static final WritePortValueAction MAPPED_WPV_1 = aSentinelWritePortValue(MONGO_WPV_1)

	private static final MongoWritePortValueAction MONGO_WPV_2 = aMongoWritePortValue(2, true)
	private static final MongoWritePortValueAction MONGO_WPV_2_UNSAVED = aMongoWritePortValue(2, false)
	private static final WritePortValueAction MAPPED_WPV_2 = aSentinelWritePortValue(MONGO_WPV_2)
	
	private static final MongoMacroAction MONGO_MAC_1 = aMongoMacroAction(1, true)
	private static final MongoMacroAction MONGO_MAC_1_UNSAVED = aMongoMacroAction(1, false)
	private static final MacroAction MAPPED_MAC_1 = aMacroAction(MONGO_MAC_1)

	private static final MongoSetZoneToSceneAction MONGO_SZ2S_1 = aMongoSetZoneToSceneAction(1, true)
	private static final MongoSetZoneToSceneAction MONGO_SZ2S_1_UNSAVED = aMongoSetZoneToSceneAction(1, false)
	private static final SetZoneToSceneAction MAPPED_SZ2S_1 = aSetZoneToSceneAction(MONGO_SZ2S_1)

	private static final MongoSetSceneActivationAction MONGO_SSA_1 = aMongoSetSceneActivationAction(1, true)
	private static final MongoSetSceneActivationAction MONGO_SSA_1_UNSAVED = aMongoSetSceneActivationAction(1, false)
	private static final SetSceneActivationAction MAPPED_SSA_1 = aSetSceneActivationAction(MONGO_SSA_1)

	EnvironmentPredicate ENVIRONMENT_PREDICATE = Stub()
	AbstractMongoEnvironmentPredicate MONGO_ENVIRONMENT_PREDICATE = Stub()

	private final MongoConditionalAction MONGO_COND_1 = aMongoConditionalAction(1, true)
	private final MongoConditionalAction MONGO_COND_1_UNSAVED = aMongoConditionalAction(1, false)
	private final ConditionalAction MAPPED_COND_1 = aConditionalAction(MONGO_COND_1)

	ZoneVariable ZONE_VARIABLE_1 = Stub()
	ZoneVariable ZONE_VARIABLE_2 = Stub()
	MongoZoneVariable MONGO_ZONE_VARIABLE_1 = Stub()
	MongoZoneVariable MONGO_ZONE_VARIABLE_2 = Stub()

	private final MongoSetZoneVariableAction MONGO_SZV_1 = aMongoSetZoneVariableAction(1, true)
	private final MongoSetZoneVariableAction MONGO_SZV_1_UNSAVED = aMongoSetZoneVariableAction(1, false)
	private final SetZoneVariableAction MAPPED_SZV_1 = aSetZoneVariableAction(MONGO_SZV_1)

	private final MongoRepublishZoneVariableAction MONGO_RZV_1 = aMongoRepublishZoneVariableAction(1, true)
	private final MongoRepublishZoneVariableAction MONGO_RZV_1_UNSAVED = aMongoRepublishZoneVariableAction(1, false)
	private final RepublishZoneVariableAction MAPPED_RZV_1 = aRepublishZoneVariableAction(MONGO_RZV_1)
	
	private final MongoRunFirstConditionalActionAction MONGO_RFCA_1 = aMongoRunFirstConditionalActionAction(1, true)
	private final MongoRunFirstConditionalActionAction MONGO_RFCA_1_UNSAVED = aMongoRunFirstConditionalActionAction(1, false)
	private final RunFirstConditionalActionAction MAPPED_RFCA_1 = aRunFirstConditionalActionAction(MONGO_RFCA_1)
	
	private final MongoCopyZoneVariableAction MONGO_CZV_1 = aMongoCopyZoneVariableAction(1, true)
	private final MongoCopyZoneVariableAction MONGO_CZV_1_UNSAVED = aMongoCopyZoneVariableAction(1, false)
	private final CopyZoneVariableAction MAPPED_CZV_1 = aCopyZoneVariableAction(MONGO_CZV_1)


	ActionMongoDao dao = Stub(ActionMongoDao) {
		save(MONGO_WPV_1_UNSAVED) >> Mono.just(MONGO_WPV_1)
		save(MONGO_MAC_1_UNSAVED) >> Mono.just(MONGO_MAC_1)
		save(MONGO_SZ2S_1_UNSAVED) >> Mono.just(MONGO_SZ2S_1)
		save(MONGO_SSA_1_UNSAVED) >> Mono.just(MONGO_SSA_1)
		save(MONGO_SZV_1_UNSAVED) >> Mono.just(MONGO_SZV_1)
		save(MONGO_RZV_1_UNSAVED) >> Mono.just(MONGO_RZV_1)
		save(MONGO_COND_1_UNSAVED) >> Mono.just(MONGO_COND_1)
		save(MONGO_RFCA_1_UNSAVED) >> Mono.just(MONGO_RFCA_1)
		save(MONGO_CZV_1_UNSAVED) >> Mono.just(MONGO_CZV_1)
		_ >> { 
			unexpectedCall(delegate) }
}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_WPV_1) >> MONGO_WPV_1
		to(MONGO_WPV_1) >> MAPPED_WPV_1
		from(MAPPED_WPV_2) >> MONGO_WPV_2
		to(MONGO_WPV_2) >> MAPPED_WPV_2
		to(A_MONGO_APPLIANCE) >> AN_APPLIANCE
		from(AN_APPLIANCE) >> A_MONGO_APPLIANCE
		from(MAPPED_MAC_1) >> MONGO_MAC_1
		to(MONGO_MAC_1) >> MAPPED_MAC_1
		
		from(ZONE_1) >> MONGO_ZONE_1
		from(SCENE) >> MONGO_SCENE
		to(MONGO_ZONE_1) >> ZONE_1
		to(MONGO_SCENE) >> SCENE
		
		from(ZONE_VARIABLE_1) >> MONGO_ZONE_VARIABLE_1
		to(MONGO_ZONE_VARIABLE_1) >> ZONE_VARIABLE_1
		from(ZONE_VARIABLE_2) >> MONGO_ZONE_VARIABLE_2
		to(MONGO_ZONE_VARIABLE_2) >> ZONE_VARIABLE_2
		
		from(ENVIRONMENT_PREDICATE) >> MONGO_ENVIRONMENT_PREDICATE
		to(MONGO_ENVIRONMENT_PREDICATE) >> ENVIRONMENT_PREDICATE
		
		from(MAPPED_COND_1) >> MONGO_COND_1
		to(MONGO_COND_1) >> MAPPED_COND_1
		
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_WPV_1.rawValue) >> A_VALUE
		writeRaw(MAPPED_WPV_1.value) >> MONGO_WPV_1.rawValue
	}

	@Subject
	ActionMongoRepository repository = new ActionMongoRepository(dao, mappers, valueConverter)

	void "Create new WritePortValueAction"() {
		expect:
		block(repository.createWritePortValueAction(MAPPED_WPV_1.name, AN_APPLIANCE, MAPPED_WPV_1.portPath, A_VALUE)) == MAPPED_WPV_1
	}

	void "Create new MacroAction"() {
		expect:
		block(repository.createMacroAction(MAPPED_MAC_1.name, [MAPPED_WPV_1, MAPPED_WPV_2])) == MAPPED_MAC_1
	}

	void "Create new SetZoneToSceneAction"() {
		expect:
		block(repository.createSetZoneToScene(MAPPED_SZ2S_1.name, ZONE_1, SCENE)) == MAPPED_SZ2S_1
	}
	
	void "Create new SetSceneActivationAction"() {
		expect:
		block(repository.createSetSceneActivation(
				MAPPED_SSA_1.name, ZONE_1, SetSceneActivationAction.Activation.ACTIVATE)) == MAPPED_SSA_1
	}

	void "Create new SetZoneVariable"() {
		expect:
		block(repository.createSetZoneVariable(
				MAPPED_SZV_1.name, ZONE_1, ZONE_VARIABLE_1, A_VALUE)) == MAPPED_SZV_1
	}

	void "Create new RepublishZoneVariable"() {
		expect:
		block(repository.createRepublishZoneVariable(
				MAPPED_RZV_1.name, ZONE_1, ZONE_VARIABLE_1)) == MAPPED_RZV_1
	}
	
	void "Create new Conditional"() {
		expect:
		block(repository.createConditional(
				MAPPED_COND_1.name, ENVIRONMENT_PREDICATE, MAPPED_WPV_1)) == MAPPED_COND_1
	}
	
	void "Create new RunFirstConditionalAction"() {
		expect:
		block(repository.createRunFirstConditionalAction(
				MAPPED_RFCA_1.name, List.of(MAPPED_COND_1))) == MAPPED_RFCA_1
	}

	void "Create new CopyZoneVariable"() {
		expect:
		block(repository.createCopyZoneVariableAction(
				MAPPED_CZV_1.name, ZONE_1, ZONE_VARIABLE_1, ZONE_2, ZONE_VARIABLE_2)) == MAPPED_CZV_1
	}


	private static MongoWritePortValueAction aMongoWritePortValue(def n, boolean saved) {
		return MongoWritePortValueAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			appliance(A_MONGO_APPLIANCE)
			portPath("port-${n}".toURI())
			rawValue("raw-value-${n}")
			build()
		}
	}

	private static WritePortValueAction aSentinelWritePortValue(MongoWritePortValueAction obj) {
		return WritePortValueAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			appliance(AN_APPLIANCE)
			portPath(obj.portPath)
			value(A_VALUE)
			build()
		}
	}
	
	private static MongoMacroAction aMongoMacroAction(def n, boolean saved) {
		return MongoMacroAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			actions([MONGO_WPV_1, MONGO_WPV_2])
			build()
		}
	}

	private static MacroAction aMacroAction(MongoMacroAction obj) {
		return MacroAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			actions([MAPPED_WPV_1, MAPPED_WPV_2])
			build()
		}
	}

	private static MongoSetZoneToSceneAction aMongoSetZoneToSceneAction(def n, boolean saved) {
		return MongoSetZoneToSceneAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zone(MONGO_ZONE_1)
			scene(MONGO_SCENE)
			build()
		}
	}

	private static SetZoneToSceneAction aSetZoneToSceneAction(MongoSetZoneToSceneAction obj) {
		return SetZoneToSceneAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zone(ZONE_1)
			scene(SCENE)
			build()
		}
	}

	private static MongoSetSceneActivationAction aMongoSetSceneActivationAction(def n, boolean saved) {
		return MongoSetSceneActivationAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zone(MONGO_ZONE_1)
			activation(SetSceneActivationAction.Activation.ACTIVATE)
			build()
		}
	}

	private static SetSceneActivationAction aSetSceneActivationAction(MongoSetSceneActivationAction obj) {
		return SetSceneActivationAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zone(ZONE_1)
			activation(obj.activation)
			build()
		}
	}
	
	private MongoSetZoneVariableAction aMongoSetZoneVariableAction(def n, boolean saved) {
		return MongoSetZoneVariableAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zone(MONGO_ZONE_1)
			zoneVariable(MONGO_ZONE_VARIABLE_1)
			rawValue("raw-value-${n}")
			build()
		}
	}

	private SetZoneVariableAction aSetZoneVariableAction(MongoSetZoneVariableAction obj) {
		return SetZoneVariableAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zone(ZONE_1)
			zoneVariable(ZONE_VARIABLE_1)
			value(A_VALUE)
			build()
		}
	}
	
	private MongoRepublishZoneVariableAction aMongoRepublishZoneVariableAction(def n, boolean saved) {
		return MongoRepublishZoneVariableAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zone(MONGO_ZONE_1)
			zoneVariable(MONGO_ZONE_VARIABLE_1)
			build()
		}
	}

	private RepublishZoneVariableAction aRepublishZoneVariableAction(MongoRepublishZoneVariableAction obj) {
		return RepublishZoneVariableAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zone(ZONE_1)
			zoneVariable(ZONE_VARIABLE_1)
			build()
		}
	}
	
	private MongoConditionalAction aMongoConditionalAction(def n, boolean saved) {
		return MongoConditionalAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			predicate(MONGO_ENVIRONMENT_PREDICATE)
			action(MONGO_WPV_1)
			build()
		}
	}

	private ConditionalAction aConditionalAction(MongoConditionalAction obj) {
		return ConditionalAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			predicate(ENVIRONMENT_PREDICATE)
			action(MAPPED_WPV_1)
			build()
		}
	}
	
	private MongoRunFirstConditionalActionAction aMongoRunFirstConditionalActionAction(def n, boolean saved) {
		return MongoRunFirstConditionalActionAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			actions([MONGO_COND_1])
			build()
		}
	}

	private RunFirstConditionalActionAction aRunFirstConditionalActionAction(MongoRunFirstConditionalActionAction obj) {
		return RunFirstConditionalActionAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			actions([MAPPED_COND_1])
			build()
		}
	}
	
	private MongoCopyZoneVariableAction aMongoCopyZoneVariableAction(def n, boolean saved) {
		return MongoCopyZoneVariableAction.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			sourceZone(MONGO_ZONE_1)
			source(MONGO_ZONE_VARIABLE_1)
			destinationZone(MONGO_ZONE_2)
			destination(MONGO_ZONE_VARIABLE_2)
			build()
		}
	}

	private CopyZoneVariableAction aCopyZoneVariableAction(MongoCopyZoneVariableAction obj) {
		return CopyZoneVariableAction.builder().with {
			id(obj.id.toString())
			name(obj.name)
			sourceZone(ZONE_1)
			source(ZONE_VARIABLE_1)
			destinationZone(ZONE_2)
			destination(ZONE_VARIABLE_2)
			build()
		}
	}
}