package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.CopyZoneVariableAction
import net.poundex.sentinel2.server.env.action.MongoCopyZoneVariableAction
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoCopyZoneVariableActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {

	private static final Zone ZONE_1 = Zone.builder().build()
	private static final Zone ZONE_2 = Zone.builder().build()
	private static final MongoZone MONGO_ZONE_1 = MongoZone.builder().build()
	private static final MongoZone MONGO_ZONE_2 = MongoZone.builder().build()
	private static final ZoneVariable ZONE_VARIABLE_1 = BooleanZoneVariable.builder().build()
	private static final MongoZoneVariable MONGO_ZONE_VARIABLE_1 = MongoBooleanZoneVariable.builder().build()
	private static final ZoneVariable ZONE_VARIABLE_2 = BooleanZoneVariable.builder().build()
	private static final MongoZoneVariable MONGO_ZONE_VARIABLE_2 = MongoBooleanZoneVariable.builder().build()
	
	static final CopyZoneVariableAction CZV = CopyZoneVariableAction.builder()
			.id(id())
			.name("szv")
			.sourceZone(ZONE_1)
			.source(ZONE_VARIABLE_1)
			.destinationZone(ZONE_2)
			.destination(ZONE_VARIABLE_2)
			.build()

	static final MongoCopyZoneVariableAction MONGO_SZV = MongoCopyZoneVariableAction.builder()
			.id(id(CZV))
			.name(CZV.name)
			.sourceZone(MONGO_ZONE_1)
			.source(MONGO_ZONE_VARIABLE_1)
			.destinationZone(MONGO_ZONE_2)
			.destination(MONGO_ZONE_VARIABLE_2)
			.build()

	MapperService mapperService = Stub() {
		to(MONGO_ZONE_1) >> ZONE_1
		from(ZONE_1) >> MONGO_ZONE_1
		to(MONGO_ZONE_2) >> ZONE_2
		from(ZONE_2) >> MONGO_ZONE_2
		
		to(MONGO_ZONE_VARIABLE_1) >> ZONE_VARIABLE_1
		from(ZONE_VARIABLE_1) >> MONGO_ZONE_VARIABLE_1
		to(MONGO_ZONE_VARIABLE_2) >> ZONE_VARIABLE_2
		from(ZONE_VARIABLE_2) >> MONGO_ZONE_VARIABLE_2
		
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoCopyZoneVariableActionMapper mapper = new MongoCopyZoneVariableActionMapper(mapperService)
	
	void "Map CopyZoneVariableAction"() {
		expect:
		mapper.from(CZV) == MONGO_SZV
		mapper.to(MONGO_SZV) == CZV
	}
}