package net.poundex.sentinel2.server.env.zonevariable

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.value.NumericValue
import net.poundex.sentinel2.server.env.value.SceneValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import static net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable.NumericFoldStrategy.AVERAGE

class MongoZoneVariableRepositorySpec extends Specification implements StrictStubbingUtils, PublisherUtils {
	
	private final static Zone A_ZONE = Zone.builder().name("some-zone").build()
	private final static MongoZone A_MONGO_ZONE = MongoZone.builder().name(A_ZONE.name).build()
	
	private static final MongoNumericZoneVariable MONGO_NZV_1 = aMongoNumericZoneVariable(1, true)
	private static final MongoNumericZoneVariable MONGO_NZV_1_UNSAVED = aMongoNumericZoneVariable(1, false)
	private static final NumericZoneVariable MAPPED_NZV_1 = aNumericWritePortValue(MONGO_NZV_1)

	private static final MongoBooleanZoneVariable MONGO_BZV_1 = aMongoBoolZoneVariable(1, true)
	private static final MongoBooleanZoneVariable MONGO_BZV_1_UNSAVED = aMongoBoolZoneVariable(1, false)
	private static final BooleanZoneVariable MAPPED_BZV_1 = aBoolWritePortValue(MONGO_BZV_1)
	
	private static final MongoSceneZoneVariable MONGO_SZV_1 = aMongoSceneZoneVariable(1, true)
	private static final MongoSceneZoneVariable MONGO_SZV_1_UNSAVED = aMongoSceneZoneVariable(1, false)
	private static final SceneZoneVariable MAPPED_SZV_1 = aSceneWritePortValue(MONGO_SZV_1)
	
	ZoneVariableMongoDao dao = Stub(ZoneVariableMongoDao) {
		save(MONGO_NZV_1_UNSAVED) >> Mono.just(MONGO_NZV_1)
		save(MONGO_BZV_1_UNSAVED) >> Mono.just(MONGO_BZV_1)
		save(MONGO_SZV_1_UNSAVED) >> Mono.just(MONGO_SZV_1)
		
		findByName(MONGO_NZV_1.name) >> Mono.just(MONGO_NZV_1)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_NZV_1) >> MONGO_NZV_1
		to(MONGO_NZV_1) >> MAPPED_NZV_1
		from(A_ZONE) >> A_MONGO_ZONE
		
		from(MAPPED_BZV_1) >> MONGO_BZV_1
		to(MONGO_BZV_1) >> MAPPED_BZV_1
		
		from(MAPPED_SZV_1) >> MONGO_SZV_1
		to(MONGO_SZV_1) >> MAPPED_SZV_1
	
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		writeRaw(MAPPED_NZV_1.defaultValue.get()) >> "number-one"
		writeRaw(MAPPED_BZV_1.defaultValue.get()) >> "boolean-false"
		writeRaw(MAPPED_SZV_1.defaultValue.get()) >> "empty-scene"
		readRaw("number-one") >> MAPPED_NZV_1.defaultValue.get()
		readRaw("boolean-false") >> MAPPED_BZV_1.defaultValue.get()
		readRaw("empty-scene") >> MAPPED_SZV_1.defaultValue.get()
		
	}
	
	@Subject
	MongoZoneVariableRepository repository = new MongoZoneVariableRepository(dao, mappers, valueConverter)

	void "Create new Numeric"() {
		given:
		dao.save(MONGO_NZV_1_UNSAVED) >> Mono.just(MONGO_NZV_1)

		expect:
		block(repository.createNumeric(
				MAPPED_NZV_1.name,
				MAPPED_NZV_1.zoneFoldStrategy, 
				MAPPED_NZV_1.defaultValue.get())) == MAPPED_NZV_1
	}
	
	void "Create new Bool"() {
		given:
		dao.save(MONGO_BZV_1_UNSAVED) >> Mono.just(MONGO_BZV_1)

		expect:
		block(repository.createBool(
				MAPPED_BZV_1.name, 
				MAPPED_BZV_1.zoneFoldStrategy, 
				MAPPED_BZV_1.defaultValue.get())) == MAPPED_BZV_1
	}
	
	void "Create new Scene"() {
		given:
		dao.save(MONGO_SZV_1_UNSAVED) >> Mono.just(MONGO_SZV_1)

		expect:
		block(repository.createScene(
				MAPPED_SZV_1.name, 
				MAPPED_SZV_1.defaultValue.get())) == MAPPED_SZV_1
	}
	
	void "Finds ZoneVariable by name"() {
		expect:
		block(repository.findByName(MAPPED_NZV_1.name)) == MAPPED_NZV_1
	}

	private static MongoNumericZoneVariable aMongoNumericZoneVariable(def n, boolean saved) {
		return MongoNumericZoneVariable.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zoneFoldStrategy(AVERAGE)
			rawDefaultValue("number-one")
			build()
		}
	}

	private static NumericZoneVariable aNumericWritePortValue(MongoNumericZoneVariable obj) {
		return NumericZoneVariable.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zoneFoldStrategy(obj.zoneFoldStrategy)
			defaultValue(Optional.of(new NumericValue(1)))
			build()
		}
	}

	private static MongoBooleanZoneVariable aMongoBoolZoneVariable(def n, boolean saved) {
		return MongoBooleanZoneVariable.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zoneFoldStrategy(BooleanZoneVariable.BooleanFoldStrategy.ANY_OF)
			rawDefaultValue("boolean-false")
			build()
		}
	}
	
	private static BooleanZoneVariable aBoolWritePortValue(MongoBooleanZoneVariable obj) {
		return BooleanZoneVariable.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zoneFoldStrategy(obj.zoneFoldStrategy)
			defaultValue(Optional.of(BooleanValue.FALSE))
			build()
		}
	}
	
	private static MongoSceneZoneVariable aMongoSceneZoneVariable(def n, boolean saved) {
		return MongoSceneZoneVariable.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			rawDefaultValue("empty-scene")
			build()
		}
	}
	
	private static SceneZoneVariable aSceneWritePortValue(MongoSceneZoneVariable obj) {
		return SceneZoneVariable.builder().with {
			id(obj.id.toString())
			name(obj.name)
			defaultValue(Optional.of(new SceneValue(null)))
			build()
		}
	}
}
