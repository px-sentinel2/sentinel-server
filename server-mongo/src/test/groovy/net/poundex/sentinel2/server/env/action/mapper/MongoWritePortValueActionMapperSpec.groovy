package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.appliance.Appliance
import net.poundex.sentinel2.server.appliance.MongoAppliance
import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction
import net.poundex.sentinel2.server.env.action.WritePortValueAction
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoWritePortValueActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {

	private static final URI PORT_PATH = "port/path".toURI()
	
	private static final Appliance APPLIANCE = Appliance.builder().build()
	private static final MongoAppliance MONGO_APPLIANCE = MongoAppliance.builder().build()
	
	private static final Zone ZONE = Zone.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()
	
	private static final ZoneVariable ZONE_VARIABLE = BooleanZoneVariable.builder().build()
	private static final MongoZoneVariable MONGO_ZONE_VARIABLE = MongoBooleanZoneVariable.builder().build()
	
	private static final Value VALUE = new Value() {}
	
	static final WritePortValueAction WPV = WritePortValueAction.builder()
			.id(id())
			.name("szv")
			.appliance(APPLIANCE)
			.portPath(PORT_PATH)
			.value(VALUE)
			.build()

	static final MongoWritePortValueAction MONGO_WPV = MongoWritePortValueAction.builder()
			.id(id(WPV))
			.name(WPV.name)
			.appliance(MONGO_APPLIANCE)
			.portPath(PORT_PATH)
			.rawValue("rv")
			.build()

	MapperService mapperService = Stub() {
		to(MONGO_ZONE) >> ZONE
		from(ZONE) >> MONGO_ZONE
		
		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
		
		to(MONGO_APPLIANCE) >> APPLIANCE
		from(APPLIANCE) >> MONGO_APPLIANCE
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub(){
		readRaw(MONGO_WPV.rawValue) >> WPV.value
		writeRaw(WPV.value) >> MONGO_WPV.rawValue
	}

	@Subject
	MongoWritePortValueActionMapper mapper = new MongoWritePortValueActionMapper(mapperService, valueConverter)
	
	void "Map WritePortValueAction"() {
		expect:
		mapper.from(WPV) == MONGO_WPV
		mapper.to(MONGO_WPV) == WPV
	}
}