package net.poundex.sentinel2.server.nest.thermostat

import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class NestThermostatMongoMapperSpec extends Specification implements MappersUtil {

	private static final NestThermostat NEST_THERMOSTAT = NestThermostat.builder()
			.id(id())
			.name("nt-1")
			.serviceUrl("nt-1-serviceurl")
			.build()

	private static final MongoNestThermostat MONGO_NEST_THERMOSTAT = MongoNestThermostat.builder()
			.id(id(NEST_THERMOSTAT))
			.name(NEST_THERMOSTAT.name)
			.serviceUrl(NEST_THERMOSTAT.serviceUrl)
			.build()

	@Subject
	NestThermostatMongoMapper mappers = new NestThermostatMongoMapper(null)

	void "Map NestThermostat"() {
		expect:
		mappers.from(NEST_THERMOSTAT) == MONGO_NEST_THERMOSTAT
		mappers.to(MONGO_NEST_THERMOSTAT) == NEST_THERMOSTAT
	}
}
