package net.poundex.sentinel2.server.env.predicate

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class EnvironmentPredicateMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {
	
	private static final Value A_VALUE = new Value() {}
	
	private static final Zone ZONE = Zone.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()
	
	private static final ZoneVariable ZONE_VARIABLE = BooleanZoneVariable.builder().build()
	private static final MongoBooleanZoneVariable MONGO_ZONE_VARIABLE = MongoBooleanZoneVariable.builder().build()

	private static final MongoZoneVariableValuePredicate MONGO_ZVV_1 = aMongoZoneVariableValuePredicate(1, true)
	private static final MongoZoneVariableValuePredicate MONGO_ZVV_1_UNSAVED = aMongoZoneVariableValuePredicate(1, false)
	private static final ZoneVariableValuePredicate MAPPED_ZVV_1 = aSentinelZoneVariableValuePredicate(MONGO_ZVV_1)

	EnvironmentPredicateMongoDao dao = Stub(EnvironmentPredicateMongoDao) {
		save(MONGO_ZVV_1_UNSAVED) >> Mono.just(MONGO_ZVV_1)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		
		from(MAPPED_ZVV_1) >> MONGO_ZVV_1
		to(MONGO_ZVV_1) >> MAPPED_ZVV_1
		
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE

		from(ZONE) >> MONGO_ZONE
		to(MONGO_ZONE) >> ZONE
		
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_ZVV_1.rawValue) >> A_VALUE
		writeRaw(MAPPED_ZVV_1.value) >> MONGO_ZVV_1.rawValue
	}

	@Subject
	EnvironmentPredicateMongoRepository repository = new EnvironmentPredicateMongoRepository(dao, mappers, valueConverter)

	void "Create new ZoneVariableValuePredicate"() {
		when:
		ZoneVariableValuePredicate<Value> zvv = block(repository.createZoneVariableValue(MAPPED_ZVV_1.name, ZONE, MAPPED_ZVV_1.zoneVariable, A_VALUE))
		
		then:
		zvv == MAPPED_ZVV_1
	}

	private static MongoZoneVariableValuePredicate aMongoZoneVariableValuePredicate(def n, boolean saved) {
		return MongoZoneVariableValuePredicate.builder().with {
			if (saved) id(new ObjectId())
			name("name-${n}")
			zone(MONGO_ZONE)
			zoneVariable(MONGO_ZONE_VARIABLE)
			rawValue("raw-value-${n}")
			build()
		}
	}

	private static ZoneVariableValuePredicate aSentinelZoneVariableValuePredicate(MongoZoneVariableValuePredicate obj) {
		return ZoneVariableValuePredicate.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zone(ZONE)
			zoneVariable(ZONE_VARIABLE)
			value(A_VALUE)
			build()
		}
	}
}
