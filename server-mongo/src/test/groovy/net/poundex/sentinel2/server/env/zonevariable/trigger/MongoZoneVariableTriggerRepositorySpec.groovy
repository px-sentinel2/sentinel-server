package net.poundex.sentinel2.server.env.zonevariable.trigger

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.AbstractMongoAction
import net.poundex.sentinel2.server.env.action.Action
import net.poundex.sentinel2.server.env.predicate.AbstractMongoEnvironmentPredicate
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.MongoNumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class MongoZoneVariableTriggerRepositorySpec extends Specification implements StrictStubbingUtils, PublisherUtils {
	
	private static final Value A_VALUE = new Value() {}
	
	private final static Zone A_ZONE = Zone.builder().name("some-zone").build()
	private final static MongoZone A_MONGO_ZONE = MongoZone.builder().name(A_ZONE.name).build()

	private final static ZoneVariable A_ZONE_VARIABLE = NumericZoneVariable.builder().name("some-zv").build()
	private final static MongoNumericZoneVariable A_MONGO_ZONE_VARIABLE =  MongoNumericZoneVariable.builder().name(A_ZONE_VARIABLE.name).build()
	
	private EnvironmentPredicate AN_ENVIRONMENT_PREDICATE = Stub()
	private AbstractMongoEnvironmentPredicate A_MONGO_ENVIRONMENT_PREDICATE = Stub()

	MongoZoneVariableTrigger MONGO_OBJ_1 = aMongoObject(1, true, false)
	MongoZoneVariableTrigger MONGO_OBJ_1_UNSAVED = aMongoObject(1, false, false)

	MongoZoneVariableTrigger MONGO_OBJ_2 = aMongoObject(2, true, true)
	MongoZoneVariableTrigger MONGO_OBJ_2_UNSAVED = aMongoObject(2, false, true)

	ZoneVariableTrigger MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	ZoneVariableTrigger MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)
	
	AbstractMongoAction mongoAction = Stub()
	Action action = Stub()

	ZoneVariableTriggerMongoDao dao = Stub(ZoneVariableTriggerMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null

		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1

		to(MONGO_OBJ_2) >> MAPPED_OBJ_2

		from(A_ZONE) >> A_MONGO_ZONE

		to(A_MONGO_ZONE_VARIABLE) >> A_ZONE_VARIABLE
		from(A_ZONE_VARIABLE) >> A_MONGO_ZONE_VARIABLE
		
		from(AN_ENVIRONMENT_PREDICATE) >> A_MONGO_ENVIRONMENT_PREDICATE
		from(null as EnvironmentPredicate) >> null
		to(A_MONGO_ENVIRONMENT_PREDICATE) >> AN_ENVIRONMENT_PREDICATE
		to(null as AbstractMongoEnvironmentPredicate) >> null
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_OBJ_1.rawValue) >> A_VALUE
		writeRaw(MAPPED_OBJ_1.value) >> MONGO_OBJ_1.rawValue
	}

	@Subject
	MongoZoneVariableTriggerRepository repository = new MongoZoneVariableTriggerRepository(dao, mappers, valueConverter)

	void "Create new"() {
		expect:
		block(repository.create(
				MAPPED_OBJ_1.name, 
				MAPPED_OBJ_1.zone,
				MAPPED_OBJ_1.zoneVariable, 
				MAPPED_OBJ_1.value, 
				MAPPED_OBJ_1.action, 
				null)) == MAPPED_OBJ_1
		
		block(repository.create(
				MAPPED_OBJ_2.name, 
				MAPPED_OBJ_2.zone, 
				MAPPED_OBJ_2.zoneVariable, 
				MAPPED_OBJ_2.value, 
				MAPPED_OBJ_2.action, 
				MAPPED_OBJ_2.predicate.get())) == MAPPED_OBJ_2
	}


	private MongoZoneVariableTrigger aMongoObject(def n, boolean saved, boolean hasPred) {
		return MongoZoneVariableTrigger.builder().with {
			if(saved) id(new ObjectId())
			name("trigger-${n}")
			zone(A_MONGO_ZONE)
			zoneVariable(A_MONGO_ZONE_VARIABLE)
			rawValue("raw-value")
			action(mongoAction)
			environmentPredicate(hasPred ? A_MONGO_ENVIRONMENT_PREDICATE : null)
			build()
		}
	}

	private ZoneVariableTrigger aSentinelObject(MongoZoneVariableTrigger obj) {
		return ZoneVariableTrigger.builder().with {
			id(obj.id.toString())
					.zoneVariable(A_ZONE_VARIABLE)
			name(obj.name)
			zone(A_ZONE)
			zoneVariable(A_ZONE_VARIABLE)
			value(A_VALUE)
			action(action)
			predicate(Optional.ofNullable(obj.getEnvironmentPredicate() == null ? null : AN_ENVIRONMENT_PREDICATE))
			build()
		}
	}
}
