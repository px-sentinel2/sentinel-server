package net.poundex.sentinel2.server.env.predicate.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction
import net.poundex.sentinel2.server.env.action.WritePortValueAction
import net.poundex.sentinel2.server.env.predicate.MongoZoneVariableValuePredicate
import net.poundex.sentinel2.server.env.predicate.ZoneVariableValuePredicate
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoZoneVariableValuePredicateMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {

	private static final ACTION = WritePortValueAction.builder().build()
	private static final MONGO_ACTION = MongoWritePortValueAction.builder().build()
	
	private static final Value A_VALUE = new Value() {}

	private static final Zone ZONE = Zone.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()

	private static final ZoneVariable ZONE_VARIABLE = BooleanZoneVariable.builder().build()
	private static final MongoBooleanZoneVariable MONGO_ZONE_VARIABLE = MongoBooleanZoneVariable.builder().build()

	static final ZoneVariableValuePredicate ZVV = ZoneVariableValuePredicate.builder()
			.id(id())
			.name("nzv")
			.zone(ZONE)
			.zoneVariable(ZONE_VARIABLE)
			.value(A_VALUE)
			.build()

	static final MongoZoneVariableValuePredicate MONGO_ZVV = MongoZoneVariableValuePredicate.builder()
			.id(id(ZVV))
			.name(ZVV.name)
			.zone(MONGO_ZONE)
			.zoneVariable(MONGO_ZONE_VARIABLE)
			.rawValue("?")
			.build()

	MapperService mapperService = Stub() {
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE

		from(ZONE) >> MONGO_ZONE
		to(MONGO_ZONE) >> ZONE
		
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_ZVV.rawValue) >> A_VALUE
		writeRaw(ZVV.value) >> MONGO_ZVV.rawValue
	}

	@Subject
	MongoZoneVariableValuePredicateMapper mapper = new MongoZoneVariableValuePredicateMapper(mapperService, valueConverter)

	void "Map ZoneVariableValuePredicate"() {
		expect:
		mapper.from(ZVV) == MONGO_ZVV
		mapper.to(MONGO_ZVV) == ZVV
	}
}
