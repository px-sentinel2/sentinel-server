package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.env.action.MacroAction
import net.poundex.sentinel2.server.env.action.MongoMacroAction
import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction
import net.poundex.sentinel2.server.env.action.WritePortValueAction
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoMacroActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {
	
	private static final ACTION = WritePortValueAction.builder().build()
	private static final MONGO_ACTION = MongoWritePortValueAction.builder().build()

	static final MacroAction MA = MacroAction.builder()
			.id(id())
			.name("nzv")
			.actions([ACTION])
			.build()

	static final MongoMacroAction MONGO_MA = MongoMacroAction.builder()
			.id(id(MA))
			.name(MA.name)
			.actions([MONGO_ACTION])
			.build()

	MapperService mapperService = Stub() {
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoMacroActionMapper mapper = new MongoMacroActionMapper(mapperService)

	void "Map MacroAction"() {
		expect:
		mapper.from(MA) == MONGO_MA
		mapper.to(MONGO_MA) == MA
	}
}
