package net.poundex.sentinel2.server.zwave.modem


import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class ZWaveModemMongoMapperSpec extends Specification implements MappersUtil {
	private static final ZWaveModem MODEM = ZWaveModem.builder()
			.id(id())
			.name("zw-1")
			.modemDevice("zw-1-dev")
			.build()

	private static final MongoZWaveModem MONGO_MODEM = MongoZWaveModem.builder()
			.id(id(MODEM))
			.name(MODEM.name)
			.modemDevice(MODEM.modemDevice)
			.build()

	@Subject
	ZWaveModemMongoMapper mappers = new ZWaveModemMongoMapper(null)

	void "Map ZWaveModem"() {
		expect:
		mappers.from(MODEM) == MONGO_MODEM
		mappers.to(MONGO_MODEM) == MODEM
	}
}