package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.env.action.ConditionalAction
import net.poundex.sentinel2.server.env.action.MongoConditionalAction
import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction
import net.poundex.sentinel2.server.env.action.WritePortValueAction
import net.poundex.sentinel2.server.env.predicate.MongoZoneVariableValuePredicate
import net.poundex.sentinel2.server.env.predicate.ZoneVariableValuePredicate
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoConditionalActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {
	
	private static final ACTION = WritePortValueAction.builder().build()
	private static final MONGO_ACTION = MongoWritePortValueAction.builder().build()
	
	private static final PREDICATE = ZoneVariableValuePredicate.builder().build()
	private static final MONGO_PREDICATE = MongoZoneVariableValuePredicate.builder().build()

	static final ConditionalAction COND = ConditionalAction.builder()
			.id(id())
			.name("nzv")
			.action(ACTION)
			.predicate(PREDICATE)
			.build()

	static final MongoConditionalAction MONGO_COND = MongoConditionalAction.builder()
			.id(id(COND))
			.name(COND.name)
			.action(MONGO_ACTION)
			.predicate(MONGO_PREDICATE)
			.build()

	MapperService mapperService = Stub() {
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		
		from(PREDICATE) >> MONGO_PREDICATE
		to(MONGO_PREDICATE) >> PREDICATE
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoConditionalActionMapper mapper = new MongoConditionalActionMapper(mapperService)

	void "Map ConditionalAction"() {
		expect:
		mapper.from(COND) == MONGO_COND
		mapper.to(MONGO_COND) == COND
	}
}
