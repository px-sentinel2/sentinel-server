package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.env.action.*
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoRunFirstConditionalActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {
	
	private static final ConditionalAction ACTION = ConditionalAction.builder().build()
	private static final AbstractMongoAction MONGO_ACTION = MongoConditionalAction.builder().build()

	static final RunFirstConditionalActionAction COND = RunFirstConditionalActionAction.builder()
			.id(id())
			.name("nzv")
			.actions([ACTION])
			.build()

	static final MongoRunFirstConditionalActionAction MONGO_COND = MongoRunFirstConditionalActionAction.builder()
			.id(id(COND))
			.name(COND.name)
			.actions([MONGO_ACTION])
			.build()

	MapperService mapperService = Stub() {
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoRunFirstConditionalActionActionMapper mapper = new MongoRunFirstConditionalActionActionMapper(mapperService)

	void "Map RunFirstConditionalActionAction"() {
		expect:
		mapper.from(COND) == MONGO_COND
		mapper.to(MONGO_COND) == COND
	}
}
