package net.poundex.sentinel2.server.util

import net.poundex.sentinel2.SentinelObject
import org.bson.types.ObjectId

trait MappersUtil {

	static String id() {
		return new ObjectId().toString()
	}

	static ObjectId id(SentinelObject obj) {
		return new ObjectId(obj.getId())
	}

}