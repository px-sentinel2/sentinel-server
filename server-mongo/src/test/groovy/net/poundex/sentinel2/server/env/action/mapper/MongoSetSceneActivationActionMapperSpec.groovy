package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.MongoSetSceneActivationAction
import net.poundex.sentinel2.server.env.action.SetSceneActivationAction
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoSetSceneActivationActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {
	
	private static final Zone ZONE = Zone.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()

	static final SetSceneActivationAction SSA = SetSceneActivationAction.builder()
			.id(id())
			.name("nzv")
			.zone(ZONE)
			.activation(SetSceneActivationAction.Activation.ACTIVATE)
			.build()

	static final MongoSetSceneActivationAction MONGO_SSA = MongoSetSceneActivationAction.builder()
			.id(id(SSA))
			.name(SSA.name)
			.zone(MONGO_ZONE)
			.activation(SetSceneActivationAction.Activation.ACTIVATE)
			.build()

	MapperService mapperService = Stub() {
		from(ZONE) >> MONGO_ZONE
		to(MONGO_ZONE) >> ZONE
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoSetSceneActivationActionMapper mapper = new MongoSetSceneActivationActionMapper(mapperService)

	void "Map SetSceneActivationAction"() {
		expect:
		mapper.from(SSA) == MONGO_SSA
		mapper.to(MONGO_SSA) == SSA
	}
	
}