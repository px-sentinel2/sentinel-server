package net.poundex.sentinel2.server.appliance

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class ApplianceMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {

	private final static Zone A_ZONE = Zone.builder().name("some-zone").build()
	private final static MongoZone A_MONGO_ZONE = MongoZone.builder().name(A_ZONE.name).build()
	
	private static final MongoAppliance MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoAppliance MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final MongoAppliance MONGO_OBJ_2 = aMongoObject(2, true)
	private static final MongoAppliance MONGO_OBJ_2_UNSAVED = aMongoObject(2, false)

	private static final Appliance MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final Appliance MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)

	ApplianceMongoDao dao = Stub(ApplianceMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		findAllByZoneAndRolesContains(A_MONGO_ZONE, [ApplianceRole.SCENE].toSet()) >>
				Flux.just(MONGO_OBJ_1)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		from(A_ZONE) >> A_MONGO_ZONE
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		_ >> { unexpectedCall(delegate) }
	}
	
	@Subject
	ApplianceMongoRepository repository = new ApplianceMongoRepository(dao, mappers)

	void "Create new"() {
		expect:
		block(repository.create(
				MAPPED_OBJ_1.name, MAPPED_OBJ_1.zone, MAPPED_OBJ_1.deviceId, MAPPED_OBJ_1.roles as ApplianceRole[])) == MAPPED_OBJ_1
	}
	
	void "Finds by zone and roles"() {
		expect:
		block(repository.findAllByZoneWithRoles(
				A_ZONE, [ApplianceRole.SCENE].toSet()).collectList()) == [MAPPED_OBJ_1]
	}
	
	private static MongoAppliance aMongoObject(def n, boolean saved) {
		return MongoAppliance.builder().with {
			if(saved) id(new ObjectId())
			name("name-${n}")
			zone(A_MONGO_ZONE)
			deviceId("device://dev${n}/self")
			roles([ApplianceRole.SCENE].toSet())
			build()
		}
	}

	private static Appliance aSentinelObject(MongoAppliance obj) {
		return Appliance.builder().with {
			id(obj.id.toString())
			name(obj.name)
			zone(A_ZONE)
			deviceId(obj.deviceId.toURI())
			roles([ApplianceRole.SCENE].toSet())
			build()
		}
	}
}
