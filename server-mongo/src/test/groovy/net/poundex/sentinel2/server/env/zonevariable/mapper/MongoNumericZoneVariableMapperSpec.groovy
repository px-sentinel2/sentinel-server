package net.poundex.sentinel2.server.env.zonevariable.mapper


import net.poundex.sentinel2.server.env.value.NumericValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.MongoNumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable
import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class MongoNumericZoneVariableMapperSpec extends Specification implements MappersUtil {

	static final NumericZoneVariable NZV = NumericZoneVariable.builder()
			.id(id())
			.name("nzv")
			.zoneFoldStrategy(NumericZoneVariable.NumericFoldStrategy.AVERAGE)
			.defaultValue(Optional.of(new NumericValue(1)))
			.build()

	static final MongoNumericZoneVariable MONGO_NZV = MongoNumericZoneVariable.builder()
			.id(id(NZV))
			.name(NZV.name)
			.zoneFoldStrategy(NZV.zoneFoldStrategy)
			.rawDefaultValue("number-one")
			.build()


	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_NZV.rawDefaultValue) >> NZV.defaultValue.get()
		writeRaw(NZV.defaultValue.get()) >> MONGO_NZV.rawDefaultValue
	}

	@Subject
	MongoNumericZoneVariableMapper mapper = new MongoNumericZoneVariableMapper(null, valueConverter)

	void "Map NumericZoneVariable"() {
		expect:
		mapper.from(NZV) == MONGO_NZV
		mapper.to(MONGO_NZV) == NZV
	}
}