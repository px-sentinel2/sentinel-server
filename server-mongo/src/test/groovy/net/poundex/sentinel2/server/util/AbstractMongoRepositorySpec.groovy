package net.poundex.sentinel2.server.util

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class AbstractMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {
	
	private static final MongoZone MONGO_OBJ_1 =
			MongoZone.builder().id(new ObjectId()).name("name-1").build()
	private static final MongoZone MONGO_OBJ_2 =
			MongoZone.builder().id(new ObjectId()).name("name-2").parent(MONGO_OBJ_1).build()
	private static final Zone MAPPED_OBJ_1 = Zone.builder().name(MONGO_OBJ_1.name).build()
	private static final Zone MAPPED_OBJ_2 = Zone.builder().name(MONGO_OBJ_2.name).build()
	
	private static class TestableAbstractMongoRepository extends AbstractMongoRepository {
		TestableAbstractMongoRepository(ReactiveMongoRepository dao, MapperService mapperService) {
			super(dao, mapperService)
		}
	}
	
	MapperService mapperService = Stub() {
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		_ >> { unexpectedCall(delegate) }
	}

	ReactiveMongoRepository dao = Stub()
	
	@Subject
	AbstractMongoRepository repository = new TestableAbstractMongoRepository(dao, mapperService)

	void "Find all"() {
		given:
		dao.findAll() >> Flux.just(MONGO_OBJ_1, MONGO_OBJ_2)

		expect:
		block(repository.findAll().collectList()) == [ MAPPED_OBJ_1, MAPPED_OBJ_2 ]
	}

	void "Find by ID"() {
		given:
		dao.findById(MONGO_OBJ_1.id) >> Mono.just(MONGO_OBJ_1)

		expect:
		block(repository.findById(MONGO_OBJ_1.id.toString())) == MAPPED_OBJ_1
	}
}
