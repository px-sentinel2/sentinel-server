package net.poundex.sentinel2.server.env.zonevariable.mapper


import net.poundex.sentinel2.server.env.value.SceneValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.MongoSceneZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.SceneZoneVariable
import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class MongoSceneZoneVariableMapperSpec extends Specification implements MappersUtil {

	static final SceneZoneVariable SZV = SceneZoneVariable.builder()
			.id(id())
			.name("nzv")
			.defaultValue(Optional.of(new SceneValue(null)))
			.build()

	static final MongoSceneZoneVariable MONGO_SZV = MongoSceneZoneVariable.builder()
			.id(id(SZV))
			.name(SZV.name)
			.rawDefaultValue("empty-scene")
			.build()

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_SZV.rawDefaultValue) >> SZV.defaultValue.get()
		writeRaw(SZV.defaultValue.get()) >> MONGO_SZV.rawDefaultValue
	}

	@Subject
	MongoSceneZoneVariableMapper mapper = new MongoSceneZoneVariableMapper(null, valueConverter)

	void "Map SceneZoneVariable"() {
		expect:
		mapper.from(SZV) == MONGO_SZV
		mapper.to(MONGO_SZV) == SZV
	}
}