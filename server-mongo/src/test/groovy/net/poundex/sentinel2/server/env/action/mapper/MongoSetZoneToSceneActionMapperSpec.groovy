package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.MongoSetZoneToSceneAction
import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction
import net.poundex.sentinel2.server.env.action.SetZoneToSceneAction
import net.poundex.sentinel2.server.env.action.WritePortValueAction
import net.poundex.sentinel2.server.env.scene.MongoScene
import net.poundex.sentinel2.server.env.scene.Scene
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoSetZoneToSceneActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {
	
	private static final ACTION = WritePortValueAction.builder().build()
	private static final MONGO_ACTION = MongoWritePortValueAction.builder().build()
	
	private static final Zone ZONE = Zone.builder().build()
	private static final Scene SCENE = Scene.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()
	private static final MongoScene MONGO_SCENE = MongoScene.builder().build()

	static final SetZoneToSceneAction MA = SetZoneToSceneAction.builder()
			.id(id())
			.name("nzv")
			.zone(ZONE)
			.scene(SCENE)
			.build()

	static final MongoSetZoneToSceneAction MONGO_MA = MongoSetZoneToSceneAction.builder()
			.id(id(MA))
			.name(MA.name)
			.zone(MONGO_ZONE)
			.scene(MONGO_SCENE)
			.build()

	MapperService mapperService = Stub() {
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		
		from(SCENE) >> MONGO_SCENE
		to(MONGO_SCENE) >> SCENE
		
		from(ZONE) >> MONGO_ZONE
		to(MONGO_ZONE) >> ZONE
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoSetZoneToSceneActionMapper mapper = new MongoSetZoneToSceneActionMapper(mapperService)

	void "Map SetZoneToSceneAction"() {
		expect:
		mapper.from(MA) == MONGO_MA
		mapper.to(MONGO_MA) == MA
	}
}