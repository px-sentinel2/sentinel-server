package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.MongoSetZoneVariableAction
import net.poundex.sentinel2.server.env.action.SetZoneVariableAction
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoSetZoneVariableActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {

	private static final Zone ZONE = Zone.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()
	
	private static final ZoneVariable ZONE_VARIABLE = BooleanZoneVariable.builder().build()
	private static final MongoZoneVariable MONGO_ZONE_VARIABLE = MongoBooleanZoneVariable.builder().build()
	
	private static final Value VALUE = new Value() {}
	
	static final SetZoneVariableAction SZV = SetZoneVariableAction.builder()
			.id(id())
			.name("szv")
			.zone(ZONE)
			.zoneVariable(ZONE_VARIABLE)
			.value(VALUE)
			.build()

	static final MongoSetZoneVariableAction MONGO_SZV = MongoSetZoneVariableAction.builder()
			.id(id(SZV))
			.name(SZV.name)
			.zone(MONGO_ZONE)
			.zoneVariable(MONGO_ZONE_VARIABLE)
			.rawValue("rv")
			.build()

	MapperService mapperService = Stub() {
		to(MONGO_ZONE) >> ZONE
		from(ZONE) >> MONGO_ZONE
		
		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub(){
		readRaw(MONGO_SZV.rawValue) >> SZV.value
		writeRaw(SZV.value) >> MONGO_SZV.rawValue
	}

	@Subject
	MongoSetZoneVariableActionMapper mapper = new MongoSetZoneVariableActionMapper(mapperService, valueConverter)
	
	void "Map SetZoneVariableAction"() {
		expect:
		mapper.from(SZV) == MONGO_SZV
		mapper.to(MONGO_SZV) == SZV
	}
}