package net.poundex.sentinel2.server.env.zonevariable.mapper

import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class MongoBooleanZoneVariableMapperSpec extends Specification implements MappersUtil {

	static final BooleanZoneVariable BZV = BooleanZoneVariable.builder()
			.id(id())
			.name("nzv")
			.zoneFoldStrategy(BooleanZoneVariable.BooleanFoldStrategy.ANY_OF)
			.defaultValue(Optional.of(BooleanValue.TRUE))
			.build()

	static final MongoBooleanZoneVariable MONGO_BZV = MongoBooleanZoneVariable.builder()
			.id(id(BZV))
			.name(BZV.name)
			.zoneFoldStrategy(BZV.zoneFoldStrategy)
			.rawDefaultValue("boolean-true")
			.build()

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_BZV.rawDefaultValue) >> BZV.defaultValue.get()
		writeRaw(BZV.defaultValue.get()) >> MONGO_BZV.rawDefaultValue
	}

	@Subject
	MongoBooleanZoneVariableMapper mapper = new MongoBooleanZoneVariableMapper(null, valueConverter)

	void "Map BooleanZoneVariable"() {
		expect:
		mapper.from(BZV) == MONGO_BZV
		mapper.to(MONGO_BZV) == BZV
	}
}

