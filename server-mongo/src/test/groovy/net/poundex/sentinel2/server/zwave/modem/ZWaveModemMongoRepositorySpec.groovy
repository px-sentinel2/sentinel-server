package net.poundex.sentinel2.server.zwave.modem


import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class ZWaveModemMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {

	private static final MongoZWaveModem MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoZWaveModem MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final MongoZWaveModem MONGO_OBJ_2 = aMongoObject(2, true)
	private static final MongoZWaveModem MONGO_OBJ_2_UNSAVED = aMongoObject(2, false)

	private static final ZWaveModem MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final ZWaveModem MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)

	ZWaveModemMongoDao dao = Stub(ZWaveModemMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	ZWaveModemMongoRepository repository = new ZWaveModemMongoRepository(dao, mappers)

	void "Create new"() {
		given:
		dao.save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)

		expect:
		block(repository.create(MONGO_OBJ_1.name, MONGO_OBJ_1.modemDevice)) == MAPPED_OBJ_1
	}
	
	private static MongoZWaveModem aMongoObject(def n, boolean saved) {
		return MongoZWaveModem.builder().with {
			if(saved) id(new ObjectId())
			name("name-${n}")
			modemDevice("dev-${n}")
			build()
		}
	}

	private static ZWaveModem aSentinelObject(MongoZWaveModem obj) {
		return ZWaveModem.builder().with {
			id(obj.id.toString())
			name(obj.name)
			modemDevice(obj.modemDevice)
			build()
		}
	}

}
