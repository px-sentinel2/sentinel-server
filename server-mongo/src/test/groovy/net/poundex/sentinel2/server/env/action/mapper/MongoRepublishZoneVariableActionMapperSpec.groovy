package net.poundex.sentinel2.server.env.action.mapper

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.action.MongoRepublishZoneVariableAction
import net.poundex.sentinel2.server.env.action.RepublishZoneVariableAction
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class MongoRepublishZoneVariableActionMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {

	private static final Zone ZONE = Zone.builder().build()
	private static final MongoZone MONGO_ZONE = MongoZone.builder().build()
	
	private static final ZoneVariable ZONE_VARIABLE = BooleanZoneVariable.builder().build()
	private static final MongoZoneVariable MONGO_ZONE_VARIABLE = MongoBooleanZoneVariable.builder().build()
	
	private static final Value VALUE = new Value() {}
	
	static final RepublishZoneVariableAction RZV = RepublishZoneVariableAction.builder()
			.id(id())
			.name("szv")
			.zone(ZONE)
			.zoneVariable(ZONE_VARIABLE)
			.build()

	static final MongoRepublishZoneVariableAction MONGO_RZV = MongoRepublishZoneVariableAction.builder()
			.id(id(RZV))
			.name(RZV.name)
			.zone(MONGO_ZONE)
			.zoneVariable(MONGO_ZONE_VARIABLE)
			.build()

	MapperService mapperService = Stub() {
		to(MONGO_ZONE) >> ZONE
		from(ZONE) >> MONGO_ZONE
		
		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
		_ >> { unexpectedCall(delegate) }
	}

	@Subject
	MongoRepublishZoneVariableActionMapper mapper = new MongoRepublishZoneVariableActionMapper(mapperService)
	
	void "Map RepublishZoneVariableAction"() {
		expect:
		mapper.from(RZV) == MONGO_RZV
		mapper.to(MONGO_RZV) == RZV
	}
}