package net.poundex.sentinel2.server.button

import net.poundex.sentinel2.server.env.action.AbstractMongoAction
import net.poundex.sentinel2.server.env.action.Action
import net.poundex.sentinel2.server.env.action.MacroAction
import net.poundex.sentinel2.server.env.action.MongoMacroAction
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class ButtonMongoMapperSpec extends Specification implements MappersUtil {

	static final Action ACTION = MacroAction.builder().build()
	static final AbstractMongoAction MONGO_ACTION = MongoMacroAction.builder().build()
	
	static final Button BUTTON = Button.builder()
			.id(id())
			.name("zone-1")
			.action(ACTION)
			.build()

	static final MongoButton MONGO_BUTTON = MongoButton.builder()
			.id(id(BUTTON))
			.name(BUTTON.name)
			.action(MONGO_ACTION)
			.build()

	MapperService mapperService = Stub() {
		to(MONGO_ACTION) >> ACTION
		from(ACTION) >> MONGO_ACTION
	}

	@Subject
	ButtonMongoMapper mapper = new ButtonMongoMapper(mapperService)

	void "Map Button"() {
		expect:
		mapper.from(BUTTON) == MONGO_BUTTON
		mapper.to(MONGO_BUTTON) == BUTTON
	}
}
