package net.poundex.sentinel2.server.env.scene

import net.poundex.sentinel2.server.env.value.ColourValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class SceneMongoMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {
	
	private static final ColourValue A_VALUE = new ColourValue(1, 2, 3)
	
	static final Scene SCENE = Scene.builder()
			.id(id())
			.name("zone-1")
			.values([A_VALUE])
			.build()

	static final MongoScene MONGO_SCENE = MongoScene.builder()
			.id(id(SCENE))
			.name(SCENE.name)
			.rawValues(["rv"])
			.build()

	MapperService mappers = Stub() {
		from(null) >> null
		from(SCENE) >> MONGO_SCENE
		to(MONGO_SCENE) >> SCENE
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_SCENE.rawValues.first()) >> A_VALUE
		writeRaw(SCENE.values.first()) >> MONGO_SCENE.rawValues.first()
	}
	
	@Subject
	SceneMongoMapper mapper = new SceneMongoMapper(mappers, valueConverter)

	void "Map Scene"() {
		expect:
		mapper.from(SCENE) == MONGO_SCENE
		mapper.to(MONGO_SCENE) == SCENE
	}
}