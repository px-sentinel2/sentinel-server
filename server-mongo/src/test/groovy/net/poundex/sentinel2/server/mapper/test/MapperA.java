package net.poundex.sentinel2.server.mapper.test;

import net.poundex.sentinel2.server.mapper.MongoMapper;

public interface MapperA extends MongoMapper<TestSentinelObjectA, TestMongoObjectA> {
}
