package net.poundex.sentinel2.server.button


import net.poundex.sentinel2.server.env.action.AbstractMongoAction
import net.poundex.sentinel2.server.env.action.Action
import net.poundex.sentinel2.server.env.action.MacroAction
import net.poundex.sentinel2.server.env.action.MongoMacroAction
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class ButtonMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {
	
	static final Action ACTION = MacroAction.builder().build()
	static final AbstractMongoAction MONGO_ACTION = MongoMacroAction.builder().build()
	
	private static final MongoButton MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoButton MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final MongoButton MONGO_OBJ_2 = aMongoObject(2, true)
	private static final MongoButton MONGO_OBJ_2_UNSAVED = aMongoObject(2, false)

	private static final Button MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final Button MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)

	ButtonMongoDao dao = Stub(ButtonMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		to(MONGO_OBJ_2) >> MAPPED_OBJ_2
		_ >> { unexpectedCall(delegate) }
	}
	
	@Subject
	ButtonMongoRepository repository = new ButtonMongoRepository(dao, mappers)

	void "Create new"() {
		expect:
		block(repository.create(MAPPED_OBJ_1.name, MAPPED_OBJ_1.action)) == MAPPED_OBJ_1
	}
	
	private static MongoButton aMongoObject(def n, boolean saved) {
		return MongoButton.builder().with {
			if(saved) id(new ObjectId())
			name("name-${n}")
			action(MONGO_ACTION)
			build()
		}
	}

	private static Button aSentinelObject(MongoButton obj) {
		return Button.builder().with {
			id(obj.id.toString())
			name(obj.name)
			action(ACTION)
			build()
		}
	}
}
