package net.poundex.sentinel2.server.env.zonevariable.trigger

import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction
import net.poundex.sentinel2.server.env.action.WritePortValueAction
import net.poundex.sentinel2.server.env.predicate.AbstractMongoEnvironmentPredicate
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.env.zonevariable.MongoNumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.ZoneMongoMapperSpec
import net.poundex.sentinel2.test.StrictStubbingUtils
import spock.lang.Specification
import spock.lang.Subject

class ZoneVariableTriggerMongoMapperSpec extends Specification implements MappersUtil, StrictStubbingUtils {

	private static final Value A_VALUE = new Value() {}
	
	private static final ACTION = WritePortValueAction.builder().build()
	private static final MONGO_ACTION = MongoWritePortValueAction.builder().build()
	private static final ZONE_VARIABLE = NumericZoneVariable.builder().build()
	private static final MONGO_ZONE_VARIABLE = MongoNumericZoneVariable.builder().build()
	
	EnvironmentPredicate ENVIRONMENT_PREDICATE = Stub()
	AbstractMongoEnvironmentPredicate MONGO_ENVIRONMENT_PREDICATE = Stub()

	private final ZoneVariableTrigger ZONE_VARIABLE_TRIGGER1 = ZoneVariableTrigger.builder()
			.id(id())
			.name("trigger1")
			.zone(ZoneMongoMapperSpec.ZONE)
			.zoneVariable(ZONE_VARIABLE)
			.value(A_VALUE)
			.action(ACTION)
			.predicate(Optional.of(ENVIRONMENT_PREDICATE))
			.build()

	private final MongoZoneVariableTrigger MONGO_ZONE_VARIABLE_TRIGGER1 = MongoZoneVariableTrigger.builder()
			.id(id(ZONE_VARIABLE_TRIGGER1))
			.name(ZONE_VARIABLE_TRIGGER1.name)
			.zone(ZoneMongoMapperSpec.MONGO_ZONE)
			.zoneVariable(MONGO_ZONE_VARIABLE)
			.rawValue("raw-value")
			.action(MONGO_ACTION)
			.environmentPredicate(MONGO_ENVIRONMENT_PREDICATE)
			.build()
	
	private final ZoneVariableTrigger ZONE_VARIABLE_TRIGGER2 = ZoneVariableTrigger.builder()
			.id(id())
			.name("trigger2")
			.zone(ZoneMongoMapperSpec.ZONE)
			.zoneVariable(ZONE_VARIABLE)
			.value(A_VALUE)
			.action(ACTION)
			.predicate(Optional.empty())
			.build()

	private final MongoZoneVariableTrigger MONGO_ZONE_VARIABLE_TRIGGER2 = MongoZoneVariableTrigger.builder()
			.id(id(ZONE_VARIABLE_TRIGGER2))
			.name(ZONE_VARIABLE_TRIGGER2.name)
			.zone(ZoneMongoMapperSpec.MONGO_ZONE)
			.zoneVariable(MONGO_ZONE_VARIABLE)
			.rawValue("raw-value")
			.action(MONGO_ACTION)
			.environmentPredicate(null)
			.build()


	MapperService mapperService = Stub() {
		to(ZoneMongoMapperSpec.MONGO_ZONE) >> ZoneMongoMapperSpec.ZONE
		from(ZoneMongoMapperSpec.ZONE) >> ZoneMongoMapperSpec.MONGO_ZONE

		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
		
		from(ACTION) >> MONGO_ACTION
		to(MONGO_ACTION) >> ACTION
		
		from(ENVIRONMENT_PREDICATE) >> MONGO_ENVIRONMENT_PREDICATE
		to(MONGO_ENVIRONMENT_PREDICATE) >> ENVIRONMENT_PREDICATE
		
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_ZONE_VARIABLE_TRIGGER1.rawValue) >> A_VALUE
		writeRaw(ZONE_VARIABLE_TRIGGER1.value) >> MONGO_ZONE_VARIABLE_TRIGGER1.rawValue
	}

	@Subject
	ZoneVariableTriggerMongoMapper mapper = new ZoneVariableTriggerMongoMapper(mapperService, valueConverter)

	void "Map ZoneVariableTrigger"() {
		expect:
		mapper.from(ZONE_VARIABLE_TRIGGER1) == MONGO_ZONE_VARIABLE_TRIGGER1
		mapper.to(MONGO_ZONE_VARIABLE_TRIGGER1) == ZONE_VARIABLE_TRIGGER1
		
		mapper.from(ZONE_VARIABLE_TRIGGER2) == MONGO_ZONE_VARIABLE_TRIGGER2
		mapper.to(MONGO_ZONE_VARIABLE_TRIGGER2) == ZONE_VARIABLE_TRIGGER2
	}
}
