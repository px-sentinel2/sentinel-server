package net.poundex.sentinel2.server.env.zonevariable.contributor

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.zonevariable.MongoNumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.zone.MongoZone
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class MongoZoneVariableContributorRepositorySpec extends Specification implements StrictStubbingUtils, PublisherUtils {
	
	private final static Zone A_ZONE = Zone.builder().name("some-zone").build()
	private final static MongoZone A_MONGO_ZONE = MongoZone.builder().name(A_ZONE.name).build()
	
	private final static ZoneVariable A_ZONE_VARIABLE = NumericZoneVariable.builder().name("some-zv").build()
	private final static MongoNumericZoneVariable A_MONGO_ZONE_VARIABLE =  MongoNumericZoneVariable.builder().name(A_ZONE_VARIABLE.name).build()
	
	private static final MongoZoneVariableContributor MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoZoneVariableContributor MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final MongoZoneVariableContributor MONGO_OBJ_2 = aMongoObject(2, true)
	private static final MongoZoneVariableContributor MONGO_OBJ_2_UNSAVED = aMongoObject(2, false)

	private static final ZoneVariableContributor MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)
	private static final ZoneVariableContributor MAPPED_OBJ_2 = aSentinelObject(MONGO_OBJ_2)

	ZoneVariableContributorMongoDao dao = Stub(ZoneVariableContributorMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		save(MONGO_OBJ_2_UNSAVED) >> Mono.just(MONGO_OBJ_2)
		findAllByPortId(MONGO_OBJ_1.portId) >> Flux.just(MONGO_OBJ_1)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null

		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1

		to(MONGO_OBJ_2) >> MAPPED_OBJ_2

		from(A_ZONE) >> A_MONGO_ZONE

		to(A_MONGO_ZONE_VARIABLE) >> A_ZONE_VARIABLE
		from(A_ZONE_VARIABLE) >> A_MONGO_ZONE_VARIABLE
		_ >> { unexpectedCall(delegate) }
	}
	
	@Subject
	MongoZoneVariableContributorRepository repository = new MongoZoneVariableContributorRepository(dao, mappers)

	void "Create new"() {
		given:
		dao.save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)

		expect:
		block(repository.create(MAPPED_OBJ_1.zoneVariable, MAPPED_OBJ_1.zone, MAPPED_OBJ_1.portId)) == MAPPED_OBJ_1
	}
	
	void "Find by port ID"() {
		when:
		List<ZoneVariableContributor> contributors =
				repository.findByPortId(MAPPED_OBJ_1.portId).collectList().block()

		then:
		contributors == [ MAPPED_OBJ_1 ]
	}
	
	private static MongoZoneVariableContributor aMongoObject(def n, boolean saved) {
		return MongoZoneVariableContributor.builder().with {
			if(saved) id(new ObjectId())
			zoneVariable(A_MONGO_ZONE_VARIABLE)
			zone(A_MONGO_ZONE)
			portId("port-${n}")
			build()
		}
	}

	private static ZoneVariableContributor aSentinelObject(MongoZoneVariableContributor obj) {
		return ZoneVariableContributor.builder().with {
			id(obj.id.toString())
			.zoneVariable(A_ZONE_VARIABLE)
			zone(A_ZONE)
			portId(obj.portId.toURI())
			build()
		}
	}
}
