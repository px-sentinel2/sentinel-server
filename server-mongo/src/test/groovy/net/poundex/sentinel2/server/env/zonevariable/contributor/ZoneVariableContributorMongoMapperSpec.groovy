package net.poundex.sentinel2.server.env.zonevariable.contributor

import net.poundex.sentinel2.server.env.zonevariable.MongoNumericZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.ZoneMongoMapperSpec
import spock.lang.Specification
import spock.lang.Subject

class ZoneVariableContributorMongoMapperSpec extends Specification implements MappersUtil {
	
	private static final ZONE_VARIABLE = NumericZoneVariable.builder().build()
	private static final MONGO_ZONE_VARIABLE = MongoNumericZoneVariable.builder().build()
	
	private static final ZoneVariableContributor ZONE_VARIABLE_CONTRIBUTOR = ZoneVariableContributor.builder()
			.id(id())
			.zone(ZoneMongoMapperSpec.ZONE)
			.portId("dev://dev/port".toURI())
			.zoneVariable(ZONE_VARIABLE)
			.build()

	private static final MongoZoneVariableContributor MONGO_ZONE_VARIABLE_CONTRIBUTOR = MongoZoneVariableContributor.builder()
			.id(id(ZONE_VARIABLE_CONTRIBUTOR))
			.zone(ZoneMongoMapperSpec.MONGO_ZONE)
			.portId(ZONE_VARIABLE_CONTRIBUTOR.portId.toString())
			.zoneVariable(MONGO_ZONE_VARIABLE)
			.build()


	MapperService mapperService = Stub() {
		to(ZoneMongoMapperSpec.MONGO_ZONE) >> ZoneMongoMapperSpec.ZONE
		from(ZoneMongoMapperSpec.ZONE) >> ZoneMongoMapperSpec.MONGO_ZONE
		
		to(MONGO_ZONE_VARIABLE) >> ZONE_VARIABLE
		from(ZONE_VARIABLE) >> MONGO_ZONE_VARIABLE
	}
	
	@Subject
	ZoneVariableContributorMongoMapper mapper = new ZoneVariableContributorMongoMapper(mapperService)
	
	void "Map ZoneVariableContributor"() {
		expect:
		mapper.from(ZONE_VARIABLE_CONTRIBUTOR) == MONGO_ZONE_VARIABLE_CONTRIBUTOR
		mapper.to(MONGO_ZONE_VARIABLE_CONTRIBUTOR) == ZONE_VARIABLE_CONTRIBUTOR
	}
}
