package net.poundex.sentinel2.server.env.scene

import net.poundex.sentinel2.server.env.value.ColourValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.test.PublisherUtils
import net.poundex.sentinel2.test.StrictStubbingUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class SceneMongoRepositorySpec extends Specification implements PublisherUtils, StrictStubbingUtils {
	
	private static final ColourValue A_VALUE = new ColourValue(1, 2, 3)
	
	private static final MongoScene MONGO_OBJ_1 = aMongoObject(1, true)
	private static final MongoScene MONGO_OBJ_1_UNSAVED = aMongoObject(1, false)

	private static final Scene MAPPED_OBJ_1 = aSentinelObject(MONGO_OBJ_1)

	SceneMongoDao dao = Stub(SceneMongoDao) {
		save(MONGO_OBJ_1_UNSAVED) >> Mono.just(MONGO_OBJ_1)
		_ >> { unexpectedCall(delegate) }
	}

	MapperService mappers = Stub() {
		from(null) >> null
		from(MAPPED_OBJ_1) >> MONGO_OBJ_1
		to(MONGO_OBJ_1) >> MAPPED_OBJ_1
		_ >> { unexpectedCall(delegate) }
	}

	ValueConverter valueConverter = Stub() {
		readRaw(MONGO_OBJ_1.rawValues.first()) >> A_VALUE
		writeRaw(MAPPED_OBJ_1.values.first()) >> MONGO_OBJ_1.rawValues.first()
	}
	
	@Subject
	SceneMongoRepository repository = new SceneMongoRepository(dao, mappers, valueConverter)

	void "Create new"() {
		expect:
		block(repository.create(MONGO_OBJ_1.name, [A_VALUE] as ColourValue[])) == MAPPED_OBJ_1
	}

	private static MongoScene aMongoObject(def n, boolean saved) {
		return MongoScene.builder().with {
			if(saved) id(new ObjectId())
			name("name-${n}")
			rawValues(["rv-${n}".toString()])
			build()
		}
	}

	private static Scene aSentinelObject(MongoScene scene) {
		return Scene.builder().with {
			id(scene.id.toString())
			name(scene.name)
			.values([A_VALUE])
			build()
		}
	}
}
