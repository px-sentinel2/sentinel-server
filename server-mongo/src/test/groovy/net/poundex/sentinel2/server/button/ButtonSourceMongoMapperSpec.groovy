package net.poundex.sentinel2.server.button

import net.poundex.sentinel2.server.env.value.ButtonValue
import net.poundex.sentinel2.server.env.value.ValueConverter
import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class ButtonSourceSourceMongoMapperSpec extends Specification implements MappersUtil {
	
	static final URI SOURCE = "dev://dev/1".toURI()
	static final ButtonValue BUTTON_VALUE = new ButtonValue(1, ButtonValue.Type.PUSHED)
	static final Button BUTTON = Button.builder().build()
	static final MongoButton MONGO_BUTTON = MongoButton.builder().build()

	static final ButtonSource BUTTON_SOURCE = ButtonSource.builder()
			.id(id())
			.button(BUTTON)
			.source(SOURCE)
			.value(BUTTON_VALUE)
			.build()

	static final MongoButtonSource MONGO_BUTTON_SOURCE = MongoButtonSource.builder()
			.id(id(BUTTON_SOURCE))
			.button(MONGO_BUTTON)
			.source(SOURCE)
			.rawValue("rv")
			.build()

	MapperService mapperService = Stub() {
		to(MONGO_BUTTON) >> BUTTON
		from(BUTTON) >> MONGO_BUTTON
	}

	ValueConverter valueConverter = Stub() {
		writeRaw(BUTTON_VALUE) >> "rv"
		readRaw("rv") >> BUTTON_VALUE
	}

	@Subject
	ButtonSourceMongoMapper mapper = new ButtonSourceMongoMapper(mapperService, valueConverter)

	void "Map ButtonSource"() {
		expect:
		mapper.from(BUTTON_SOURCE) == MONGO_BUTTON_SOURCE
		mapper.to(MONGO_BUTTON_SOURCE) == BUTTON_SOURCE
	}
}
