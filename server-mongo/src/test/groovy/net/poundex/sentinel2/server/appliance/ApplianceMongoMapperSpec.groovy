package net.poundex.sentinel2.server.appliance


import net.poundex.sentinel2.server.mapper.MapperService
import net.poundex.sentinel2.server.util.MappersUtil
import net.poundex.sentinel2.server.zone.ZoneMongoMapperSpec
import spock.lang.Specification
import spock.lang.Subject

class ApplianceMongoMapperSpec extends Specification implements MappersUtil {

	static final Appliance APPLIANCE = Appliance.builder()
			.id(id())
			.name("zone-1")
			.zone(ZoneMongoMapperSpec.ZONE)
			.deviceId("device://device/self".toURI())
			.roles([ApplianceRole.SCENE].toSet())
			.build()

	static final MongoAppliance MONGO_APPLIANCE = MongoAppliance.builder()
			.id(id(APPLIANCE))
			.name(APPLIANCE.name)
			.zone(ZoneMongoMapperSpec.MONGO_ZONE)
			.deviceId(APPLIANCE.deviceId.toString())
			.roles([ApplianceRole.SCENE].toSet())
			.build()

	MapperService mapperService = Stub() {
		to(ZoneMongoMapperSpec.MONGO_ZONE) >> ZoneMongoMapperSpec.ZONE
		from(ZoneMongoMapperSpec.ZONE) >> ZoneMongoMapperSpec.MONGO_ZONE
	}

	@Subject
	ApplianceMongoMapper mapper = new ApplianceMongoMapper(mapperService)

	void "Map Appliance"() {
		expect:
		mapper.from(APPLIANCE) == MONGO_APPLIANCE
		mapper.to(MONGO_APPLIANCE) == APPLIANCE
	}
}
