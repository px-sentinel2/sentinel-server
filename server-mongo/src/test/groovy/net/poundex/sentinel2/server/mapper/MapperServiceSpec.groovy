package net.poundex.sentinel2.server.mapper

import net.poundex.sentinel2.SentinelObject
import net.poundex.sentinel2.server.MongoObject
import net.poundex.sentinel2.server.mapper.test.MapperA
import net.poundex.sentinel2.server.mapper.test.MapperB
import net.poundex.sentinel2.server.mapper.test.TestMongoObjectA
import net.poundex.sentinel2.server.mapper.test.TestMongoObjectB
import net.poundex.sentinel2.server.mapper.test.TestSentinelObjectA
import net.poundex.sentinel2.server.mapper.test.TestSentinelObjectB
import org.springframework.beans.factory.ObjectProvider
import spock.lang.Specification

class MapperServiceSpec extends Specification {
	
	SentinelObject objA = new TestSentinelObjectA()
	MongoObject mobjA = new TestMongoObjectA()
	SentinelObject objB = new TestSentinelObjectB()
	MongoObject mobjB = new TestMongoObjectB()
	
	MapperA mapperA = Stub(MapperA) {
		from(objA) >> mobjA
		to(mobjA) >> objA
	}
	
	MapperB mapperB = Stub(MapperB) {
		from(objB) >> mobjB
		to(mobjB) >> objB
	}

	ObjectProvider<MongoMapper<?, ?>> mappers = Stub() {
		stream() >> { [mapperA, mapperB].stream() }
	}
	
	MapperService service = new MapperService(mappers)
	
	void "Finds correct mapper and returns mapped value"() {
		expect:
		service.from(objA) == mobjA
		service.from(objB) == mobjB
		
		service.to(mobjA) == objA
		service.to(mobjB) == objB
	}
	
	void "Returns null if input is null"() {
		expect:
		service.from(null) == null
		service.to(null) == null
	}
}
