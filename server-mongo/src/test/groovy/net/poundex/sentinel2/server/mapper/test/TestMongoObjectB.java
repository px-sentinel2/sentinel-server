package net.poundex.sentinel2.server.mapper.test;

import net.poundex.sentinel2.server.MongoObject;
import org.bson.types.ObjectId;

public class TestMongoObjectB implements MongoObject {
	@Override
	public ObjectId getId() {
		throw new UnsupportedOperationException();
	}
}
