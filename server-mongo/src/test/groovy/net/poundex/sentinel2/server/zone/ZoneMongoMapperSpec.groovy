package net.poundex.sentinel2.server.zone

import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class ZoneMongoMapperSpec extends Specification implements MappersUtil {

	static final Zone ZONE = Zone.builder()
			.id(id())
			.name("zone-1")
			.parent(Optional.empty())
			.build()

	static final MongoZone MONGO_ZONE = MongoZone.builder()
			.id(id(ZONE))
			.name(ZONE.name)
			.parent(null)
			.build()

	@Subject
	ZoneMongoMapper mapper = new ZoneMongoMapper(null)

	void "Map Zone"() {
		expect:
		mapper.from(ZONE) == MONGO_ZONE
		mapper.to(MONGO_ZONE) == ZONE
	}
}
