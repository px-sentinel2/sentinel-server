package net.poundex.sentinel2.server.hue.bridge

import net.poundex.sentinel2.server.util.MappersUtil
import spock.lang.Specification
import spock.lang.Subject

class HueBridgeMongoMapperSpec extends Specification implements MappersUtil {
	private static final HueBridge HUE_BRIDGE = HueBridge.builder()
			.id(id())
			.name("hb-1")
			.address("hb-1-address")
			.build()

	private static final MongoHueBridge MONGO_HUE_BRIDGE = MongoHueBridge.builder()
			.id(id(HUE_BRIDGE))
			.name(HUE_BRIDGE.name)
			.address(HUE_BRIDGE.address)
			.build()

	@Subject
	HueBridgeMongoMapper mappers = new HueBridgeMongoMapper(null)

	void "Map HueBridge"() {
		expect:
		mappers.from(HUE_BRIDGE) == MONGO_HUE_BRIDGE
		mappers.to(MONGO_HUE_BRIDGE) == HUE_BRIDGE
	}
}