package net.poundex.sentinel2.server.button;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.MongoObject;
import net.poundex.sentinel2.server.env.action.AbstractMongoAction;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("button")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
public class MongoButton implements MongoObject {

	@Id
	private ObjectId id;

	private String name;
	private AbstractMongoAction action;
}
