package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.MongoRepublishZoneVariableAction;
import net.poundex.sentinel2.server.env.action.RepublishZoneVariableAction;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoRepublishZoneVariableActionMapper extends AbstractMongoMapper<RepublishZoneVariableAction, MongoRepublishZoneVariableAction> {
	
	public MongoRepublishZoneVariableActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public RepublishZoneVariableAction to(MongoRepublishZoneVariableAction obj) {
		return RepublishZoneVariableAction.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.zoneVariable(toOther(obj.getZoneVariable()))
				.build();
	}

	@Override
	public MongoRepublishZoneVariableAction from(RepublishZoneVariableAction obj) {
		return MongoRepublishZoneVariableAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.zoneVariable(fromOther(obj.getZoneVariable()))
				.build();
	}
}
