package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.MongoSetZoneToSceneAction;
import net.poundex.sentinel2.server.env.action.SetZoneToSceneAction;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoSetZoneToSceneActionMapper extends AbstractMongoMapper<SetZoneToSceneAction, MongoSetZoneToSceneAction> {
	
	public MongoSetZoneToSceneActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public SetZoneToSceneAction to(MongoSetZoneToSceneAction obj) {
		return SetZoneToSceneAction.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.scene(toOther(obj.getScene()))
				.build(); 
	}

	@Override
	public MongoSetZoneToSceneAction from(SetZoneToSceneAction obj) {
		return MongoSetZoneToSceneAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.scene(fromOther(obj.getScene()))
				.build();
	}
}
