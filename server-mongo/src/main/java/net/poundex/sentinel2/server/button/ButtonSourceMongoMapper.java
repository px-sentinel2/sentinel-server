package net.poundex.sentinel2.server.button;

import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

@Component
class ButtonSourceMongoMapper extends AbstractMongoMapper<ButtonSource, MongoButtonSource> {

	private final ValueConverter valueConverter;

	public ButtonSourceMongoMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	public MongoButtonSource from(ButtonSource obj) {
		return MongoButtonSource.builder()
				.id(id(obj.getId()))
				.button(fromOther(obj.getButton()))
				.source(obj.getSource())
				.rawValue(valueConverter.writeRaw(obj.getValue()))
				.build();
	}

	public ButtonSource to(MongoButtonSource obj) {
		return ButtonSource.builder()
				.id(id(obj.getId()))
				.button(toOther(obj.getButton()))
				.source(obj.getSource())
				.value(valueConverter.readRaw(obj.getRawValue()))
				.build();
	}
}
