package net.poundex.sentinel2.server.button;

import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
class ButtonMongoRepository extends AbstractMongoRepository<Button, MongoButton, ButtonMongoDao> implements ButtonRepository {

	public ButtonMongoRepository(ButtonMongoDao dao, MapperService mapperService) {
		super(dao, mapperService);
	}

	@Override
	public Mono<Button> create(String name, Action action) {
		return dao
				.save(newPersistent(name, action))
				.map(mapperService::to);
	}
	private MongoButton newPersistent(String name, Action action) {
		return MongoButton.builder()
				.name(name)
				.action(mapperService.from(action))
				.build();
	}
}