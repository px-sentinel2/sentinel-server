package net.poundex.sentinel2.server.appliance;

import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
class ApplianceMongoMapper extends AbstractMongoMapper<Appliance, MongoAppliance> {

	public ApplianceMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoAppliance from(Appliance obj) {
		return MongoAppliance.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.deviceId(obj.getDeviceId().toString())
				.roles(obj.getRoles())
				.build();
	}

	public Appliance to(MongoAppliance obj) {
		return Appliance.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.deviceId(URI.create(obj.getDeviceId()))
				.roles(obj.getRoles())
				.build();
	}

}
