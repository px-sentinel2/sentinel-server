package net.poundex.sentinel2.server.env.zonevariable;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("zoneVariable")
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MongoBooleanZoneVariable 
		extends MongoZoneVariable<BooleanValue, BooleanZoneVariable, BooleanZoneVariable.BooleanFoldStrategy> {

	private BooleanZoneVariable.BooleanFoldStrategy zoneFoldStrategy;
}
