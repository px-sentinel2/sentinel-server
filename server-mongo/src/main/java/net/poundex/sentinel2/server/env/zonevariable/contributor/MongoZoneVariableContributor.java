package net.poundex.sentinel2.server.env.zonevariable.contributor;

import lombok.*;
import net.poundex.sentinel2.server.MongoObject;
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable;
import net.poundex.sentinel2.server.zone.MongoZone;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("zoneVariableContributor")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Builder
public class MongoZoneVariableContributor implements MongoObject {

	@Id
	private ObjectId id;

	private MongoZoneVariable<?, ?, ?> zoneVariable;
	private MongoZone zone;
	private String portId;
}
