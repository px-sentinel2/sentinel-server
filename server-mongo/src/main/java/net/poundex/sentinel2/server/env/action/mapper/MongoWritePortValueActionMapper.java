package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.MongoWritePortValueAction;
import net.poundex.sentinel2.server.env.action.WritePortValueAction;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoWritePortValueActionMapper extends AbstractMongoMapper<WritePortValueAction<?>, MongoWritePortValueAction> {
	
	private final ValueConverter valueConverter;
	
	public MongoWritePortValueActionMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public WritePortValueAction<?> to(MongoWritePortValueAction obj) {
		return doMapTo(obj);
	}
	
	private <VT extends Value<VT>> WritePortValueAction<VT> doMapTo(MongoWritePortValueAction obj) {
		return WritePortValueAction.<VT>builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.appliance(toOther(obj.getAppliance()))
				.portPath(obj.getPortPath())
				.value(valueConverter.readRaw(obj.getRawValue()))
				.build();
	}

	@Override
	public MongoWritePortValueAction from(WritePortValueAction<?> obj) {
		return MongoWritePortValueAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.appliance(fromOther(obj.getAppliance()))
				.portPath(obj.getPortPath())
				.rawValue(valueConverter.writeRaw(obj.getValue()))
				.build();
	}
}
