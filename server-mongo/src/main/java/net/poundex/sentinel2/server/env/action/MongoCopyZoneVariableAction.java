package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable;
import net.poundex.sentinel2.server.zone.MongoZone;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoCopyZoneVariableAction extends AbstractMongoAction {
	private MongoZone sourceZone;
	private MongoZoneVariable<?, ?, ?> source;
	private MongoZone destinationZone;
	private MongoZoneVariable<?, ?, ?> destination;
}
