package net.poundex.sentinel2.server.env.zonevariable.trigger;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.MongoObject;
import net.poundex.sentinel2.server.env.action.AbstractMongoAction;
import net.poundex.sentinel2.server.env.predicate.AbstractMongoEnvironmentPredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.zone.MongoZone;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("trigger")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
public class MongoZoneVariableTrigger<VT extends Value<VT>, T extends ZoneVariable<VT>> implements MongoObject {

	@Id
	private ObjectId id;

	private String name;
	
	private MongoZone zone;
	private MongoZoneVariable<VT, T, ?> zoneVariable;
	private String rawValue;
	private AbstractMongoAction action;
	private AbstractMongoEnvironmentPredicate environmentPredicate;
}
