package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.appliance.MongoAppliance;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoWritePortValueAction extends AbstractMongoAction {

	private MongoAppliance appliance;
	private URI portPath;
	private String rawValue;
}
