package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoRunFirstConditionalActionAction extends AbstractMongoAction {
	private List<AbstractMongoAction> actions;
}
