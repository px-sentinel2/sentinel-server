package net.poundex.sentinel2.server.env.scene;

import lombok.*;
import net.poundex.sentinel2.server.MongoObject;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("scene")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Builder
public class MongoScene implements MongoObject {
	@Id
	private ObjectId id;
	
	private String name;
	private List<String> rawValues;
}
