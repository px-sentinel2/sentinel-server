package net.poundex.sentinel2.server.mapper;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.MongoObject;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Service;

import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unchecked")
@Service
public class MapperService {
	
	private final Supplier<Stream<MongoMapper<?, ?>>> mappersSupplier;
	
	private final LoadingCache<Class<?>, MongoMapper<?, ?>> mapperCache = Caffeine.newBuilder()
			.build(this::getMapperFor);

	public MapperService(ObjectProvider<MongoMapper<?, ?>> mappersProvider) {
		this.mappersSupplier = mappersProvider::stream;
	}

	public <T extends SentinelObject, MT extends MongoObject> MT from(T obj) {
		if(obj == null) return null;
		return ((MongoMapper<T, MT>) mapperCache.get(obj.getClass())).from(obj);
	}

	public <T extends SentinelObject, MT extends MongoObject> T to(MT obj) {
		if(obj == null) return null;
		return ((MongoMapper<T, MT>) mapperCache.get(obj.getClass())).to(obj);
	}

	private <T extends SentinelObject, MT extends MongoObject> MongoMapper<T, MT> getMapperFor(Class<?> klass) {
		return (MongoMapper<T, MT>) mappersSupplier.get()
				.filter(onType(klass))
				.findFirst()
				.orElseThrow(() -> new NoSuchElementException("Could not find mapper for " + klass.getName()));
	}

	private <T extends SentinelObject> Predicate<? super MongoMapper<?, ?>> onType(Class<?> klass) {
		return mapper -> getMapperPair((Class<MongoMapper<T, ?>>) mapper.getClass()).accepts(klass);
	}

	private record MapperPair<T extends SentinelObject, MT extends MongoObject>(
			Class<T> sentinelObjectType,
			Class<MT> monboObjectType) {

		public  boolean accepts (Class<?> klass) {
			return klass.equals(sentinelObjectType) || klass.equals(monboObjectType);
		}
	}
	
	private <T extends SentinelObject, MT extends MongoObject> MapperPair<T, MT> getMapperPair(Class<? super MongoMapper<T, MT>> klass) {
		Map<GenericDeclaration, ? extends Class<?>> types = GenericTypeResolver.getTypeVariableMap(klass)
				.entrySet()
				.stream()
				.filter(kv -> kv.getKey().getGenericDeclaration().equals(MongoMapper.class))
				.map(kv -> {
					if(kv.getValue() instanceof ParameterizedType pt)
						return Map.entry(kv.getKey(), pt.getRawType());
					return kv;
				})
				.collect(Collectors.toMap(
						kv -> SentinelObject.class.isAssignableFrom((Class<?>) kv.getValue()) 
								? SentinelObject.class 
								: MongoObject.class,
						kv -> (Class<?>) kv.getValue()));

		return new MapperPair<>(
				((Class<T>) types.get(SentinelObject.class)),
				((Class<MT>) types.get(MongoObject.class)));
	}

}
