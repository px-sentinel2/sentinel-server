package net.poundex.sentinel2.server.env.zonevariable.trigger;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
class MongoZoneVariableTriggerRepository extends AbstractMongoRepository<ZoneVariableTrigger<?, ?>, MongoZoneVariableTrigger<?, ?>, ZoneVariableTriggerMongoDao> implements ZoneVariableTriggerRepository {

	private final ValueConverter valueConverter;
	
	public MongoZoneVariableTriggerRepository(ZoneVariableTriggerMongoDao dao, MapperService mapperService, ValueConverter valueConverter) {
		super(dao, mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public <VT extends Value<VT>, T extends ZoneVariable<VT>> Mono<ZoneVariableTrigger<VT, T>> create(
			String name, Zone zone, T zoneVariable, VT value, Action action, EnvironmentPredicate environmentPredicate) {
		return dao
				.save(newPersistent(name, zone, zoneVariable, value, action, environmentPredicate))
				.map(mapperService::to);
	}
	
	@Override
	public <VT extends Value<VT>, T extends ZoneVariable<VT>> Flux<ZoneVariableTrigger<VT, T>> findByZoneAndZoneVariable(Zone zone, T zoneVariable) {
		return dao.findAllByZoneAndZoneVariable(mapperService.from(zone), mapperService.from(zoneVariable))
				.map(mapperService::to);
	}

	private <VT extends Value<VT>, T extends ZoneVariable<VT>> MongoZoneVariableTrigger<VT, T> newPersistent(
			String name, Zone zone, T zoneVariable, VT value, Action action, EnvironmentPredicate environmentPredicate) {
		return MongoZoneVariableTrigger.<VT, T>builder()
				.name(name)
				.zone(mapperService.from(zone))
				.zoneVariable(mapperService.from(zoneVariable))
				.rawValue(valueConverter.writeRaw(value))
				.action(mapperService.from(action))
				.environmentPredicate(mapperService.from(environmentPredicate))
				.build();
	}
}
