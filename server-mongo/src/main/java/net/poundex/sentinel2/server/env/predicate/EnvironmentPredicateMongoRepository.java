package net.poundex.sentinel2.server.env.predicate;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
class EnvironmentPredicateMongoRepository extends AbstractMongoRepository<EnvironmentPredicate, AbstractMongoEnvironmentPredicate, EnvironmentPredicateMongoDao> implements EnvironmentPredicateRepository {

	private final ValueConverter valueConverter;

	public EnvironmentPredicateMongoRepository(EnvironmentPredicateMongoDao dao, MapperService mapperService, ValueConverter valueConverter) {
		super(dao, mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public <VT extends Value<VT>> Mono<ZoneVariableValuePredicate<VT>> createZoneVariableValue(
			String name, Zone zone, ZoneVariable<VT> zoneVariable, VT value) {
		return dao.save(MongoZoneVariableValuePredicate.builder()
						.name(name)
						.zone(mapperService.from(zone))
						.zoneVariable(mapperService.from(zoneVariable))
						.rawValue(valueConverter.writeRaw(value))
						.build())
				.map(obj -> ZoneVariableValuePredicate.<VT>builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.zone(mapperService.to(obj.getZone()))
						.zoneVariable(mapperService.to(obj.getZoneVariable()))
						.value(valueConverter.readRaw(obj.getRawValue()))
						.build());
	}
}
