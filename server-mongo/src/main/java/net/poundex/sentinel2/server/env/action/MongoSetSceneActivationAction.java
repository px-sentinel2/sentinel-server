package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.zone.MongoZone;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoSetSceneActivationAction extends AbstractMongoAction {
	private MongoZone zone;
	private SetSceneActivationAction.Activation activation;
}
