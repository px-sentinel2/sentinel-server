package net.poundex.sentinel2.server.button;

import net.poundex.sentinel2.server.env.value.ButtonValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.net.URI;

@Service
class ButtonSourceMongoRepository extends AbstractMongoRepository<ButtonSource, MongoButtonSource, ButtonSourceMongoDao> implements ButtonSourceRepository {

	private final ValueConverter valueConverter;
	
	public ButtonSourceMongoRepository(ButtonSourceMongoDao dao, MapperService mapperService, ValueConverter valueConverter) {
		super(dao, mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public Mono<ButtonSource> create(Button button, URI source, ButtonValue value) {
		return dao
				.save(newPersistent(button, source, value))
				.map(mapperService::to);
	}

	@Override
	public Mono<ButtonSource> findBySourceAndValue(URI source, ButtonValue buttonValue) {
		return dao.findBySourceAndRawValue(source, valueConverter.writeRaw(buttonValue))
				.map(mapperService::to);
	}

	private MongoButtonSource newPersistent(Button button, URI source, ButtonValue value) {
		return MongoButtonSource.builder()
				.button(mapperService.from(button))
				.source(source)
				.rawValue(valueConverter.writeRaw(value))
				.build();
	}
}