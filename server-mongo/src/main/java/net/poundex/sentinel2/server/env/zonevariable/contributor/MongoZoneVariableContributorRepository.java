package net.poundex.sentinel2.server.env.zonevariable.contributor;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

@Service
class MongoZoneVariableContributorRepository extends AbstractMongoRepository<ZoneVariableContributor, MongoZoneVariableContributor, ZoneVariableContributorMongoDao> implements ZoneVariableContributorRepository {

	public MongoZoneVariableContributorRepository(ZoneVariableContributorMongoDao dao, MapperService mapperService) {
		super(dao, mapperService);
	}

	@Override
	public Mono<ZoneVariableContributor> create(ZoneVariable<?> zoneVariable, Zone zone, URI portId) {
		return dao
				.save(newPersistent(zoneVariable, zone, portId))
				.map(mapperService::to);
	}
	
	private MongoZoneVariableContributor newPersistent(ZoneVariable<?> zoneVariable, Zone zone, URI portId) {
		return MongoZoneVariableContributor.builder()
				.zoneVariable(mapperService.from(zoneVariable))
				.zone(mapperService.from(zone))
				.portId(portId.toString())
				.build();
	}
	
	@Override
	public Flux<ZoneVariableContributor> findByPortId(URI portId) {
		return dao.findAllByPortId(portId.toString()).map(mapperService::to);
	}
}
