package net.poundex.sentinel2.server.hue.bridge;

import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

@Component
class HueBridgeMongoMapper extends AbstractMongoMapper<HueBridge, MongoHueBridge> {

	public HueBridgeMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoHueBridge from(HueBridge obj) {
		return MongoHueBridge.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.address(obj.getAddress())
				.build();
	}
	
	public HueBridge to(MongoHueBridge obj) {
		return HueBridge.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.address(obj.getAddress())
				.build();
	}
}
