package net.poundex.sentinel2.server.util;

import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.MongoObject;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class AbstractMongoRepository<T extends SentinelObject, MT extends MongoObject, R extends ReactiveMongoRepository<MT, ObjectId>> 
		implements ReadOnlyRepository<T>, MongoUtils {
	
	protected final R dao;
	protected final MapperService mapperService;

	@Override
	public Flux<T> findAll() {
		return dao.findAll().map(mapperService::to);
	}

	@Override
	public Mono<T> findById(String id) {
		return withValidId(id, oid -> dao.findById(oid).map(mapperService::to));
	}

	@Override
	public Flux<T> findAllById(Iterable<String> ids) {
		return withValidIds(ids, oids -> dao.findAllById(oids).map(mapperService::to));
	}
}
