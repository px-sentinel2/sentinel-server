package net.poundex.sentinel2.server.hue.bridge;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface HueBridgeMongoDao extends ReactiveMongoRepository<MongoHueBridge, ObjectId> {
	
}
