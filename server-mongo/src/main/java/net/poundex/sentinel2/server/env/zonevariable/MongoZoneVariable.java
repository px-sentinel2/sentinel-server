package net.poundex.sentinel2.server.env.zonevariable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.MongoObject;
import net.poundex.sentinel2.server.env.value.Value;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("zoneVariable")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
public abstract class MongoZoneVariable<VT extends Value<VT>, T extends ZoneVariable<VT>, F extends FoldStrategy<VT>> implements MongoObject {

	@Id
	private ObjectId id;

	private String name;
	private String rawDefaultValue;
	
	abstract F getZoneFoldStrategy();
}
