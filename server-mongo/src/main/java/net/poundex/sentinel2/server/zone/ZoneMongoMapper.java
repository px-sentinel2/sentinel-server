package net.poundex.sentinel2.server.zone;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.zone.MongoZone;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class ZoneMongoMapper extends AbstractMongoMapper<Zone, MongoZone> {

	public ZoneMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoZone from(Zone obj) {
		return MongoZone.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.parent(obj.getParent().map(this::from).orElse(null))
				.build();
	}

	public Zone to(MongoZone obj) {
		return Zone.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.parent(Optional.ofNullable(obj.getParent()).map(this::to))
				.build();
	}
}
