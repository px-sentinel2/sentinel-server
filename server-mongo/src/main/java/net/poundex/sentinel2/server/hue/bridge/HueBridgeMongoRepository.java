package net.poundex.sentinel2.server.hue.bridge;

import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class HueBridgeMongoRepository extends AbstractMongoRepository<HueBridge, MongoHueBridge, HueBridgeMongoDao> implements HueBridgeRepository {

	public HueBridgeMongoRepository(HueBridgeMongoDao dao, MapperService mapperService) {
		super(dao, mapperService);
	}

	@Override
	public Mono<HueBridge> create(String name, String address) {
		return dao
				.save(newPersistent(name, address))
				.map(mapperService::to);
	}
	
	private MongoHueBridge newPersistent(String name, String address) {
		return MongoHueBridge.builder()
				.name(name)
				.address(address)
				.build();
	}
}
