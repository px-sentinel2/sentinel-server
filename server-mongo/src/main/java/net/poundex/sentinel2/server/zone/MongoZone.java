package net.poundex.sentinel2.server.zone;

import lombok.*;
import net.poundex.sentinel2.server.MongoObject;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("zone")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Builder
public class MongoZone implements MongoObject {
	@Id
	private ObjectId id;
	
	private String name;
	private MongoZone parent;
}
