package net.poundex.sentinel2.server.nest.thermostat;

import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

@Component
class NestThermostatMongoMapper extends AbstractMongoMapper<NestThermostat, MongoNestThermostat> {

	public NestThermostatMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoNestThermostat from(NestThermostat obj) {
		return MongoNestThermostat.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.serviceUrl(obj.getServiceUrl())
				.build();
	}

	public NestThermostat to(MongoNestThermostat obj) {
		return NestThermostat.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.serviceUrl(obj.getServiceUrl())
				.build();
	}
}
