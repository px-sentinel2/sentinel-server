package net.poundex.sentinel2.server.env.predicate.mapper;

import net.poundex.sentinel2.server.env.predicate.MongoZoneVariableValuePredicate;
import net.poundex.sentinel2.server.env.predicate.ZoneVariableValuePredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoZoneVariableValuePredicateMapper extends AbstractMongoMapper<ZoneVariableValuePredicate<?>, MongoZoneVariableValuePredicate> {
	
	private final ValueConverter valueConverter;

	public MongoZoneVariableValuePredicateMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public ZoneVariableValuePredicate<?> to(MongoZoneVariableValuePredicate obj) {
		return doMapTo(obj);
	}

	@Override
	public MongoZoneVariableValuePredicate from(ZoneVariableValuePredicate<?> obj) {
		return doMapFrom(obj);
	}

	private <VT extends Value<VT>> ZoneVariableValuePredicate<VT> doMapTo(MongoZoneVariableValuePredicate obj) {
		return ZoneVariableValuePredicate.<VT>builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.zoneVariable(toOther(obj.getZoneVariable()))
				.value(valueConverter.readRaw(obj.getRawValue()))
				.build();
	}

	private <VT extends Value<VT>> MongoZoneVariableValuePredicate doMapFrom(ZoneVariableValuePredicate<VT> obj) {
		return MongoZoneVariableValuePredicate.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.zoneVariable(fromOther(obj.getZoneVariable()))
				.rawValue(valueConverter.writeRaw(obj.getValue()))
				.build();
	}
}