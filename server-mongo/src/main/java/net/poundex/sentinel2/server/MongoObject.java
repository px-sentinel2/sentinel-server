package net.poundex.sentinel2.server;

import org.bson.types.ObjectId;

public interface MongoObject {
	ObjectId getId();
}
