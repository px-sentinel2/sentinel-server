package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.scene.MongoScene;
import net.poundex.sentinel2.server.zone.MongoZone;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoSetZoneToSceneAction extends AbstractMongoAction {
	private MongoZone zone;
	private MongoScene scene;
}
