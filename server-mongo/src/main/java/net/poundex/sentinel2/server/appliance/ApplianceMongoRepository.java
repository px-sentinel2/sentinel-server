package net.poundex.sentinel2.server.appliance;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class ApplianceMongoRepository  extends AbstractMongoRepository<Appliance, MongoAppliance, ApplianceMongoDao> implements ApplianceRepository {

	public ApplianceMongoRepository(ApplianceMongoDao dao, MapperService mapperService) {
		super(dao, mapperService);
	}

	@Override
	public Mono<Appliance> create(String name, Zone zone, URI deviceId, ApplianceRole... roles) {
		return dao
				.save(newPersistent(name, zone, deviceId, 
						Arrays.stream(roles).collect(Collectors.toSet())))
				.map(mapperService::to);
	}

	@Override
	public Flux<Appliance> findAllByZoneWithRoles(Zone zone, Set<ApplianceRole> roles) {
		return dao.findAllByZoneAndRolesContains(mapperService.from(zone), roles)
				.map(mapperService::to);
	}

	private MongoAppliance newPersistent(String name, Zone zone, URI deviceId, Set<ApplianceRole> roles) {
		return MongoAppliance.builder()
				.name(name)
				.zone(mapperService.from(zone))
				.deviceId(deviceId.toString())
				.roles(roles)
				.build();
	}
}