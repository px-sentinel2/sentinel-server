package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.MongoSetSceneActivationAction;
import net.poundex.sentinel2.server.env.action.SetSceneActivationAction;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoSetSceneActivationActionMapper extends AbstractMongoMapper<SetSceneActivationAction, MongoSetSceneActivationAction> {
	
	public MongoSetSceneActivationActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public SetSceneActivationAction to(MongoSetSceneActivationAction obj) {
		return SetSceneActivationAction.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.activation(obj.getActivation())
				.build(); 
	}

	@Override
	public MongoSetSceneActivationAction from(SetSceneActivationAction obj) {
		return MongoSetSceneActivationAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.activation(obj.getActivation())
				.build();
	}
}
