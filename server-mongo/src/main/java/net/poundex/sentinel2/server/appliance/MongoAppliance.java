package net.poundex.sentinel2.server.appliance;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.MongoObject;
import net.poundex.sentinel2.server.zone.MongoZone;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document("appliance")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
public class MongoAppliance implements MongoObject {

	@Id
	private ObjectId id;

	private String name;
	private MongoZone zone;
	private String deviceId;
	private Set<ApplianceRole> roles;
}
