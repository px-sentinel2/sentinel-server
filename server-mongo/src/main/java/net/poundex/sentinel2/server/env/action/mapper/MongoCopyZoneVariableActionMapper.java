package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.CopyZoneVariableAction;
import net.poundex.sentinel2.server.env.action.MongoCopyZoneVariableAction;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoCopyZoneVariableActionMapper extends AbstractMongoMapper<CopyZoneVariableAction<?>, MongoCopyZoneVariableAction> {
	
	
	public MongoCopyZoneVariableActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public CopyZoneVariableAction<?> to(MongoCopyZoneVariableAction obj) {
		return doMapTo(obj);
	}
	
	private <VT extends Value<VT>> CopyZoneVariableAction<VT> doMapTo(MongoCopyZoneVariableAction obj) {
		return CopyZoneVariableAction.<VT>builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.sourceZone(toOther(obj.getSourceZone()))
				.source(toOther(obj.getSource()))
				.destinationZone(toOther(obj.getDestinationZone()))
				.destination(toOther(obj.getDestination()))
				.build();
	}

	@Override
	public MongoCopyZoneVariableAction from(CopyZoneVariableAction<?> obj) {
		return MongoCopyZoneVariableAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.sourceZone(fromOther(obj.getSourceZone()))
				.source(fromOther(obj.getSource()))
				.destinationZone(fromOther(obj.getDestinationZone()))
				.destination(fromOther(obj.getDestination()))
				.build();
	}
}
