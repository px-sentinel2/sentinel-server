package net.poundex.sentinel2.server.button;

import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

@Component
class ButtonMongoMapper extends AbstractMongoMapper<Button, MongoButton> {

	public ButtonMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoButton from(Button obj) {
		return MongoButton.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.action(fromOther(obj.getAction()))
				.build();
	}

	public Button to(MongoButton obj) {
		return Button.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.action(toOther(obj.getAction()))
				.build();
	}

}
