package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.MongoSetZoneVariableAction;
import net.poundex.sentinel2.server.env.action.SetZoneVariableAction;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoSetZoneVariableActionMapper extends AbstractMongoMapper<SetZoneVariableAction<?>, MongoSetZoneVariableAction> {
	
	private final ValueConverter valueConverter;
	
	public MongoSetZoneVariableActionMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public SetZoneVariableAction<?> to(MongoSetZoneVariableAction obj) {
		return doMapTo(obj);
	}
	
	private <VT extends Value<VT>> SetZoneVariableAction<VT> doMapTo(MongoSetZoneVariableAction obj) {
		return SetZoneVariableAction.<VT>builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.zoneVariable(toOther(obj.getZoneVariable()))
				.value(valueConverter.readRaw(obj.getRawValue()))
				.build();
	}

	@Override
	public MongoSetZoneVariableAction from(SetZoneVariableAction<?> obj) {
		return MongoSetZoneVariableAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.zoneVariable(fromOther(obj.getZoneVariable()))
				.rawValue(valueConverter.writeRaw(obj.getValue()))
				.build();
	}
}
