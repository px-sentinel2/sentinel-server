package net.poundex.sentinel2.server.env.predicate;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface EnvironmentPredicateMongoDao extends ReactiveMongoRepository<AbstractMongoEnvironmentPredicate, ObjectId> {
	
}
