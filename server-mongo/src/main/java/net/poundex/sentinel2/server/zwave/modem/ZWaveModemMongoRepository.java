package net.poundex.sentinel2.server.zwave.modem;

import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ZWaveModemMongoRepository extends AbstractMongoRepository<ZWaveModem, MongoZWaveModem, ZWaveModemMongoDao> implements ZWaveModemRepository {

	public ZWaveModemMongoRepository(ZWaveModemMongoDao dao, MapperService mapperService) {
		super(dao, mapperService);
	}

	@Override
	public Mono<ZWaveModem> create(String name, String address) {
		return dao
				.save(newPersistent(name, address))
				.map(mapperService::to);
	}
	
	private MongoZWaveModem newPersistent(String name, String modemDevice) {
		return MongoZWaveModem.builder()
				.name(name)
				.modemDevice(modemDevice)
				.build();
	}
}
