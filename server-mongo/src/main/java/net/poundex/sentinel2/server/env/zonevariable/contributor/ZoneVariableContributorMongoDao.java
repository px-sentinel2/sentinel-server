package net.poundex.sentinel2.server.env.zonevariable.contributor;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface ZoneVariableContributorMongoDao extends ReactiveMongoRepository<MongoZoneVariableContributor, ObjectId> {
	Flux<MongoZoneVariableContributor> findAllByPortId(String portId);
}
