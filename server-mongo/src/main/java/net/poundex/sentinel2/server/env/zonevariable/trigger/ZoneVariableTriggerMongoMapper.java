package net.poundex.sentinel2.server.env.zonevariable.trigger;

import net.poundex.sentinel2.server.env.predicate.AbstractMongoEnvironmentPredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class ZoneVariableTriggerMongoMapper extends AbstractMongoMapper<ZoneVariableTrigger<?, ?>, MongoZoneVariableTrigger<?, ?>> {
	
	private final ValueConverter valueConverter;
	
	public ZoneVariableTriggerMongoMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public ZoneVariableTrigger<?, ?> to(MongoZoneVariableTrigger<?, ?> obj) {
		return doMapTo(obj);
	}

	@Override
	public MongoZoneVariableTrigger<?, ?> from(ZoneVariableTrigger<?, ?> obj) {
		return doMapFrom(obj);
	}

	private <VT extends Value<VT>, T extends ZoneVariable<VT>> MongoZoneVariableTrigger<VT, T> doMapFrom(ZoneVariableTrigger<VT, T> obj) {
		return MongoZoneVariableTrigger.<VT, T>builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.zone(fromOther(obj.getZone()))
				.zoneVariable(fromOther(obj.getZoneVariable()))
				.rawValue(valueConverter.writeRaw(obj.getValue()))
				.action(fromOther(obj.getAction()))
				.environmentPredicate(obj.getPredicate().map(sentinelObject -> 
						(AbstractMongoEnvironmentPredicate) fromOther(sentinelObject))
						.orElse(null))
				.build();
	}

	private <VT extends Value<VT>, T extends ZoneVariable<VT>> ZoneVariableTrigger<VT, T> doMapTo(MongoZoneVariableTrigger<VT, T> obj) {
		return ZoneVariableTrigger.<VT, T>builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.zone(toOther(obj.getZone()))
				.zoneVariable(toOther(obj.getZoneVariable()))
				.value(valueConverter.readRaw(obj.getRawValue()))
				.action(toOther(obj.getAction()))
				.predicate(Optional.ofNullable(obj.getEnvironmentPredicate()).map(this::toOther))
				.build();
	}

}
