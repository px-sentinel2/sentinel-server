package net.poundex.sentinel2.server.env.zonevariable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.value.NumericValue;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("zoneVariable")
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@Getter
public class MongoNumericZoneVariable 
		extends MongoZoneVariable<NumericValue, NumericZoneVariable, NumericZoneVariable.NumericFoldStrategy> {

	private NumericZoneVariable.NumericFoldStrategy zoneFoldStrategy;
}
