package net.poundex.sentinel2.server.env.zonevariable;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

interface ZoneVariableMongoDao extends ReactiveMongoRepository<MongoZoneVariable<?, ?, ?>, ObjectId> {

	Mono<MongoZoneVariable<?, ?, ?>> findByName(String name);
}
