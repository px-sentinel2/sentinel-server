package net.poundex.sentinel2.server.env.scene;

import net.poundex.sentinel2.server.env.value.ColourValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

@Component
class SceneMongoMapper extends AbstractMongoMapper<Scene, MongoScene> {
	
	private final ValueConverter valueConverter;
	
	public SceneMongoMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	public MongoScene from(Scene obj) {
		return MongoScene.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.rawValues(obj.getValues().stream().map(valueConverter::writeRaw).toList())
				.build();
	}

	public Scene to(MongoScene obj) {
		return Scene.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.values(obj.getRawValues().stream()
						.map(x -> (ColourValue) valueConverter.readRaw(x)).toList())
				.build();
	}
}
