package net.poundex.sentinel2.server.env.scene;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface SceneMongoDao extends ReactiveMongoRepository<MongoScene, ObjectId> {
	
}
