package net.poundex.sentinel2.server.env.zonevariable.mapper;

import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable;
import net.poundex.sentinel2.server.env.zonevariable.MongoBooleanZoneVariable;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class MongoBooleanZoneVariableMapper extends AbstractMongoMapper<BooleanZoneVariable, MongoBooleanZoneVariable> {
	
	private final ValueConverter valueConverter;
	
	public MongoBooleanZoneVariableMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public BooleanZoneVariable to(MongoBooleanZoneVariable obj) {
		return BooleanZoneVariable.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zoneFoldStrategy(obj.getZoneFoldStrategy())
				.defaultValue(Optional.<BooleanValue>ofNullable(valueConverter.readRaw(obj.getRawDefaultValue())))
				.build();
	}

	@Override
	public MongoBooleanZoneVariable from(BooleanZoneVariable obj) {
		return MongoBooleanZoneVariable.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zoneFoldStrategy(obj.getZoneFoldStrategy())
				.rawDefaultValue(valueConverter.writeRaw(obj.getDefaultValue().orElse(null)))
				.build();
	}
}
