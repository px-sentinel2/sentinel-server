package net.poundex.sentinel2.server.button;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

import java.net.URI;

interface ButtonSourceMongoDao extends ReactiveMongoRepository<MongoButtonSource, ObjectId> {
	Mono<MongoButtonSource> findBySourceAndRawValue(URI source, String rawValue);
}
