package net.poundex.sentinel2.server.env.zonevariable.mapper;

import net.poundex.sentinel2.server.env.value.SceneValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.MongoSceneZoneVariable;
import net.poundex.sentinel2.server.env.zonevariable.SceneZoneVariable;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class MongoSceneZoneVariableMapper extends AbstractMongoMapper<SceneZoneVariable, MongoSceneZoneVariable> {
	
	private final ValueConverter valueConverter;
	
	public MongoSceneZoneVariableMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public SceneZoneVariable to(MongoSceneZoneVariable obj) {
		return SceneZoneVariable.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.defaultValue(Optional.<SceneValue>ofNullable(valueConverter.readRaw(obj.getRawDefaultValue())))
				.build();
	}

	@Override
	public MongoSceneZoneVariable from(SceneZoneVariable obj) {
		return MongoSceneZoneVariable.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.rawDefaultValue(valueConverter.writeRaw(obj.getDefaultValue().orElse(null)))
				.build();
	}
}
