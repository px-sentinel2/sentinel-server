package net.poundex.sentinel2.server.env.predicate;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.action.AbstractMongoAction;
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable;
import net.poundex.sentinel2.server.zone.MongoZone;

import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoZoneVariableValuePredicate extends AbstractMongoEnvironmentPredicate {
	private MongoZone zone;
	private MongoZoneVariable<?, ?, ?> zoneVariable;
	private String rawValue;
}
