package net.poundex.sentinel2.server.nest.thermostat;

import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
class NestThermostatMongoRepository extends AbstractMongoRepository<NestThermostat, MongoNestThermostat, NestThermostatMongoDao> implements NestThermostatRepository {
	
	private final NestCloudService nestService;

	public NestThermostatMongoRepository(NestThermostatMongoDao dao, MapperService mapperService, NestCloudService nestService) {
		super(dao, mapperService);
		this.nestService = nestService;
	}

	@Override
	public Mono<NestThermostat> create(String name) {
		return dao.save(newPersistent(name)).map(mapperService::to);
	}

	private MongoNestThermostat newPersistent(String name) {
		return MongoNestThermostat.builder()
				.name(name)
				.serviceUrl(nestService.getServiceUrl(name).orElse(null))
				.build();
	}
}
