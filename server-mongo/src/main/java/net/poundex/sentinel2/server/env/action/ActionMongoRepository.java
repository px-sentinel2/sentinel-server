package net.poundex.sentinel2.server.env.action;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.appliance.Appliance;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.scene.Scene;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

@Service
class ActionMongoRepository extends AbstractMongoRepository<Action, AbstractMongoAction, ActionMongoDao> implements ActionRepository {
	
	private final ValueConverter valueConverter;

	public ActionMongoRepository(ActionMongoDao dao, MapperService mapperService, ValueConverter valueConverter) {
		super(dao, mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public <VT extends Value<VT>> Mono<WritePortValueAction<VT>> createWritePortValueAction(String name, Appliance appliance, URI portPath, VT value) {
		return dao.save(MongoWritePortValueAction.builder()
						.name(name)
						.appliance(mapperService.from(appliance))
						.portPath(portPath)
						.rawValue(valueConverter.writeRaw(value))
						.build())
				.map(obj -> WritePortValueAction.<VT>builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.appliance(mapperService.to(obj.getAppliance()))
						.portPath(obj.getPortPath())
						.value(valueConverter.readRaw(obj.getRawValue()))
						.build());
	}

	@Override
	public Mono<MacroAction> createMacroAction(String name, List<? extends Action> actions) {
		return dao.save(MongoMacroAction.builder()
						.name(name)
						.actions(actions
								.stream()
								.map(x -> (AbstractMongoAction) mapperService.from(x))
								.toList())
						.build())
				.map(obj -> MacroAction.builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.actions(obj.getActions().stream().map(x -> (Action) mapperService.to(x)).toList())
						.build());
	}

	@Override
	public Mono<SetZoneToSceneAction> createSetZoneToScene(String name, Zone zone, Scene scene) {
		return dao.save(MongoSetZoneToSceneAction.builder()
						.name(name)
						.zone(mapperService.from(zone))
						.scene(mapperService.from(scene))
						.build())
				.map(obj -> SetZoneToSceneAction.builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.zone(mapperService.to(obj.getZone()))
						.scene(mapperService.to(obj.getScene()))
						.build());
	}

	@Override
	public Mono<SetSceneActivationAction> createSetSceneActivation(String name, Zone zone, SetSceneActivationAction.Activation activation) {
		return dao.save(MongoSetSceneActivationAction.builder()
						.name(name)
						.zone(mapperService.from(zone))
						.activation(activation)
						.build())
				.map(obj -> SetSceneActivationAction.builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.zone(mapperService.to(obj.getZone()))
						.activation(obj.getActivation())
						.build());
	}

	@Override
	public <VT extends Value<VT>> Mono<SetZoneVariableAction<VT>> createSetZoneVariable(String name, Zone zone, ZoneVariable<VT> zoneVariable, VT value) {
		return dao.save(MongoSetZoneVariableAction.builder()
						.name(name)
						.zone(mapperService.from(zone))
						.zoneVariable(mapperService.from(zoneVariable))
						.rawValue(valueConverter.writeRaw(value))
						.build())
				.map(obj -> SetZoneVariableAction.<VT>builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.zone(mapperService.to(obj.getZone()))
						.zoneVariable(mapperService.to(obj.getZoneVariable()))
						.value(valueConverter.readRaw(obj.getRawValue()))
						.build());
	}

	@Override
	public Mono<RepublishZoneVariableAction> createRepublishZoneVariable(
			String name, Zone zone, ZoneVariable<?> zoneVariable) {
		return dao.save(MongoRepublishZoneVariableAction.builder()
						.name(name)
						.zone(mapperService.from(zone))
						.zoneVariable(mapperService.from(zoneVariable))
						.build())
				.map(obj -> RepublishZoneVariableAction.builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.zone(mapperService.to(obj.getZone()))
						.zoneVariable(mapperService.to(obj.getZoneVariable()))
						.build());
	}

	@Override
	public Mono<ConditionalAction> createConditional(String name, EnvironmentPredicate predicate, Action action) {
		return dao.save(MongoConditionalAction.builder()
						.name(name)
						.predicate(mapperService.from(predicate))
						.action(mapperService.from(action))
						.build())
				.map(obj -> ConditionalAction.builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.predicate(mapperService.to(obj.getPredicate()))
						.action(mapperService.to(obj.getAction()))
						.build());
	}

	@Override
	public Mono<RunFirstConditionalActionAction> createRunFirstConditionalAction(String name, List<ConditionalAction> actions) {
		return dao.save(MongoRunFirstConditionalActionAction.builder()
						.name(name)
						.actions(actions
								.stream()
								.map(x -> (AbstractMongoAction) mapperService.from(x))
								.toList())
						.build())
				.map(obj -> RunFirstConditionalActionAction.builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.actions(obj.getActions().stream().map(x -> 
								(ConditionalAction) mapperService.to(x)).toList())
						.build());
	}

	@Override
	public <VT extends Value<VT>> Mono<CopyZoneVariableAction<VT>> createCopyZoneVariableAction(String name, Zone sourceZone, ZoneVariable<VT> source, Zone destinationZone, ZoneVariable<VT> destination) {
		return dao.save(MongoCopyZoneVariableAction.builder()
						.name(name)
						.sourceZone(mapperService.from(sourceZone))
						.source(mapperService.from(source))
						.destinationZone(mapperService.from(destinationZone))
						.destination(mapperService.from(destination))
						.build())
				.map(obj -> CopyZoneVariableAction.<VT>builder()
						.id(obj.getId().toString())
						.name(obj.getName())
						.sourceZone(mapperService.to(obj.getSourceZone()))
						.source(mapperService.to(obj.getSource()))
						.destinationZone(mapperService.to(obj.getDestinationZone()))
						.destination(mapperService.to(obj.getDestination()))
						.build());
	}
}
