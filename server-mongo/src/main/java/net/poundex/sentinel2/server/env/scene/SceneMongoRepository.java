package net.poundex.sentinel2.server.env.scene;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.ColourValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import net.poundex.sentinel2.server.util.MongoUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Service
class SceneMongoRepository extends AbstractMongoRepository<Zone, MongoScene, SceneMongoDao> 
		implements SceneRepository, MongoUtils {
	
	private final ValueConverter valueConverter;
	
	public SceneMongoRepository(SceneMongoDao dao, MapperService mapperService, ValueConverter valueConverter) {
		super(dao, mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public Mono<Scene> create(String name, ColourValue... colourValues) {
		return dao
				.save(newPersistent(name, 
						Arrays.stream(colourValues).map(valueConverter::writeRaw).toList()))
				.map(mapperService::to);
	}

	private MongoScene newPersistent(String name, List<String> values) {
		return MongoScene.builder()
				.name(name)
				.rawValues(values)
				.build();
	}
}
