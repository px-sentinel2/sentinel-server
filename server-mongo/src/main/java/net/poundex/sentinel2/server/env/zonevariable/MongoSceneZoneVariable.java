package net.poundex.sentinel2.server.env.zonevariable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.value.SceneValue;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("zoneVariable")
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
public class MongoSceneZoneVariable 
		extends MongoZoneVariable<SceneValue, SceneZoneVariable, FoldStrategy<SceneValue>> {

	@Override
	FoldStrategy<SceneValue> getZoneFoldStrategy() {
		return null;
	}
}
