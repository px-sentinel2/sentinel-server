package net.poundex.sentinel2.server.zone;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import net.poundex.sentinel2.server.util.MongoUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
class ZoneMongoRepository extends AbstractMongoRepository<Zone, MongoZone, ZoneMongoDao> implements ZoneRepository, MongoUtils {
	
	public ZoneMongoRepository(ZoneMongoDao dao, MapperService mapperService) {
		super(dao, mapperService);
	}
	
	@Override
	public Mono<Zone> create(String name, Zone parent) {
		return dao
				.save(newPersistent(name, mapperService.from(parent)))
				.map(mapperService::to);
	}

	private MongoZone newPersistent(String name, MongoZone parent) {
		return MongoZone.builder()
				.name(name)
				.parent(parent)
				.build();
	}
}
