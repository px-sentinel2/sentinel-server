package net.poundex.sentinel2.server.nest.thermostat;

import lombok.*;
import net.poundex.sentinel2.server.MongoObject;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("nestThermostat")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Builder
class MongoNestThermostat implements MongoObject {

	@Id
	private ObjectId id;

	private String name;
	private String serviceUrl;
}
