package net.poundex.sentinel2.server.zone;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface ZoneMongoDao extends ReactiveMongoRepository<MongoZone, ObjectId> {
	
}
