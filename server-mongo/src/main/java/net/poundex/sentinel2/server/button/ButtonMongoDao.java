package net.poundex.sentinel2.server.button;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface ButtonMongoDao extends ReactiveMongoRepository<MongoButton, ObjectId> {
}
