package net.poundex.sentinel2.server.zwave.modem;

import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

@Component
class ZWaveModemMongoMapper extends AbstractMongoMapper<ZWaveModem, MongoZWaveModem> {

	public ZWaveModemMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoZWaveModem from(ZWaveModem obj) {
		return MongoZWaveModem.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.modemDevice(obj.getModemDevice())
				.build();
	}
	
	public ZWaveModem to(MongoZWaveModem obj) {
		return ZWaveModem.builder()
				.id(id(obj.getId()))
				.name(obj.getName())
				.modemDevice(obj.getModemDevice())
				.build();
	}
}
