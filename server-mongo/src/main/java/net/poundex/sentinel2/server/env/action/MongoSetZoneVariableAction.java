package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable;
import net.poundex.sentinel2.server.zone.MongoZone;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoSetZoneVariableAction extends AbstractMongoAction {
	private MongoZone zone;
	private MongoZoneVariable<?, ?, ?> zoneVariable;
	private String rawValue;
}
