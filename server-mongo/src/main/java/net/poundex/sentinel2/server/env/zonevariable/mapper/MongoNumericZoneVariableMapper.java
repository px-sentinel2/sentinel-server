package net.poundex.sentinel2.server.env.zonevariable.mapper;

import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.env.zonevariable.MongoNumericZoneVariable;
import net.poundex.sentinel2.server.env.zonevariable.NumericZoneVariable;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class MongoNumericZoneVariableMapper extends AbstractMongoMapper<NumericZoneVariable, MongoNumericZoneVariable> {
	
	private final ValueConverter valueConverter;
	
	public MongoNumericZoneVariableMapper(MapperService mapperService, ValueConverter valueConverter) {
		super(mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public NumericZoneVariable to(MongoNumericZoneVariable obj) {
		return NumericZoneVariable.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.zoneFoldStrategy(obj.getZoneFoldStrategy())
				.defaultValue(Optional.<NumericValue>ofNullable(valueConverter.readRaw(obj.getRawDefaultValue())))
				.build();
	}

	@Override
	public MongoNumericZoneVariable from(NumericZoneVariable obj) {
		return MongoNumericZoneVariable.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.zoneFoldStrategy(obj.getZoneFoldStrategy())
				.rawDefaultValue(valueConverter.writeRaw(obj.getDefaultValue().orElse(null)))
				.build();
	}
}
