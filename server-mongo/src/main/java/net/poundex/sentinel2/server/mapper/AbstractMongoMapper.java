package net.poundex.sentinel2.server.mapper;

import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.MongoObject;
import org.bson.types.ObjectId;

@RequiredArgsConstructor
public abstract class AbstractMongoMapper<T extends SentinelObject, MT extends MongoObject> 
		implements MongoMapper<T, MT> {

	private final MapperService mapperService;

	protected <T2 extends SentinelObject> T2 toOther(MongoObject mongoObject) {
		return mapperService.to(mongoObject);
	}
	
	protected <T2 extends MongoObject> T2 fromOther(SentinelObject sentinelObject) {
		return mapperService.from(sentinelObject);
	}
	
	protected ObjectId id(String id) {
		return new ObjectId(id);
	}

	protected String id(ObjectId id) {
		return id.toString();
	}
}
