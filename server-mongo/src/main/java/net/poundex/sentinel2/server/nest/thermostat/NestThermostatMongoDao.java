package net.poundex.sentinel2.server.nest.thermostat;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface NestThermostatMongoDao extends ReactiveMongoRepository<MongoNestThermostat, ObjectId> { }
