package net.poundex.sentinel2.server.env.zonevariable.trigger;

import net.poundex.sentinel2.server.env.zonevariable.MongoZoneVariable;
import net.poundex.sentinel2.server.zone.MongoZone;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface ZoneVariableTriggerMongoDao extends ReactiveMongoRepository<MongoZoneVariableTrigger<?, ?>, ObjectId> {
	Flux<MongoZoneVariableTrigger<?, ?>> findAllByZoneAndZoneVariable(MongoZone zone, MongoZoneVariable<?, ?, ?> zoneVariable);
}
