package net.poundex.sentinel2.server.appliance;

import net.poundex.sentinel2.server.zone.MongoZone;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

import java.util.Set;

interface ApplianceMongoDao extends ReactiveMongoRepository<MongoAppliance, ObjectId> {
	Flux<MongoAppliance> findAllByZoneAndRolesContains(MongoZone zone, Set<ApplianceRole> roles);
}
