package net.poundex.sentinel2.server.zwave.modem;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface ZWaveModemMongoDao extends ReactiveMongoRepository<MongoZWaveModem, ObjectId> {
	
}
