package net.poundex.sentinel2.server.hue.bridge;

import lombok.*;
import net.poundex.sentinel2.server.MongoObject;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("hueBridge")
@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Builder
class MongoHueBridge implements MongoObject {

	@Id
	private ObjectId id;

	private String name;
	private String address;
}
