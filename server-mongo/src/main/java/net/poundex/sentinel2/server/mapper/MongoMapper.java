package net.poundex.sentinel2.server.mapper;

import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.MongoObject;

public interface MongoMapper<T extends SentinelObject, MT extends MongoObject> {
	T to(MT obj);
	MT from(T obj);

}
