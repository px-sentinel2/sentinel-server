package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.AbstractMongoAction;
import net.poundex.sentinel2.server.env.action.ConditionalAction;
import net.poundex.sentinel2.server.env.action.MongoRunFirstConditionalActionAction;
import net.poundex.sentinel2.server.env.action.RunFirstConditionalActionAction;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoRunFirstConditionalActionActionMapper extends AbstractMongoMapper<RunFirstConditionalActionAction, MongoRunFirstConditionalActionAction> {
	
	public MongoRunFirstConditionalActionActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public RunFirstConditionalActionAction to(MongoRunFirstConditionalActionAction obj) {
		return RunFirstConditionalActionAction.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.actions(obj.getActions().stream().map(ma -> (ConditionalAction) toOther(ma)).toList())
				.build();
	}

	@Override
	public MongoRunFirstConditionalActionAction from(RunFirstConditionalActionAction obj) {
		return MongoRunFirstConditionalActionAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.actions(obj.getActions().stream().map(a -> (AbstractMongoAction) fromOther(a)).toList())
				.build();
	}
}
