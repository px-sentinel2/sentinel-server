package net.poundex.sentinel2.server.env.zonevariable;

import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.env.value.SceneValue;
import net.poundex.sentinel2.server.env.value.ValueConverter;
import net.poundex.sentinel2.server.mapper.MapperService;
import net.poundex.sentinel2.server.util.AbstractMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
class MongoZoneVariableRepository extends AbstractMongoRepository<ZoneVariable<?>, MongoZoneVariable<?, ?, ?>, ZoneVariableMongoDao> implements ZoneVariableRepository {

	private final ValueConverter valueConverter;
	
	public MongoZoneVariableRepository(ZoneVariableMongoDao dao, MapperService mapperService, ValueConverter valueConverter) {
		super(dao, mapperService);
		this.valueConverter = valueConverter;
	}

	@Override
	public Mono<NumericZoneVariable> createNumeric(
			String name,
			NumericZoneVariable.NumericFoldStrategy zoneFoldStrategy,
			NumericValue defaultValue) {
		return dao.save(MongoNumericZoneVariable.builder()
						.name(name)
						.zoneFoldStrategy(zoneFoldStrategy)
						.rawDefaultValue(valueConverter.writeRaw(defaultValue))
						.build())
				.map(mzv -> NumericZoneVariable.builder()
						.id(mzv.getId().toString())
						.name(mzv.getName())
						.zoneFoldStrategy(zoneFoldStrategy)
						.defaultValue(Optional.<NumericValue>ofNullable(valueConverter.readRaw(mzv.getRawDefaultValue())))
						.build());
	}

	@Override
	public Mono<BooleanZoneVariable> createBool(
			String name, 
			BooleanZoneVariable.BooleanFoldStrategy foldStrategy,
			BooleanValue defaultValue) {
		return dao.save(MongoBooleanZoneVariable.builder()
						.name(name)
						.zoneFoldStrategy(foldStrategy)
						.rawDefaultValue(valueConverter.writeRaw(defaultValue))
						.build())
				.map(mzv -> BooleanZoneVariable.builder()
						.id(mzv.getId().toString())
						.name(mzv.getName())
						.zoneFoldStrategy(foldStrategy)
						.defaultValue(Optional.<BooleanValue>ofNullable(valueConverter.readRaw(mzv.getRawDefaultValue())))
						.build());
	}

	@Override
	public Mono<SceneZoneVariable> createScene(String name, SceneValue defaultValue) {
		return dao.save(MongoSceneZoneVariable.builder()
						.name(name)
						.rawDefaultValue(valueConverter.writeRaw(defaultValue))
						.build())
				.map(mzv -> SceneZoneVariable.builder()
						.id(mzv.getId().toString())
						.name(mzv.getName())
						.defaultValue(Optional.<SceneValue>ofNullable(valueConverter.readRaw(mzv.getRawDefaultValue())))
						.build());
	}

	@Override
	public <T extends ZoneVariable<?>> Mono<T> findByName(String name) {
		return dao
				.findByName(name)
				.map(mapperService::to);
	}
}
