package net.poundex.sentinel2.server.env.action;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface ActionMongoDao extends ReactiveMongoRepository<AbstractMongoAction, ObjectId> {
	
}
