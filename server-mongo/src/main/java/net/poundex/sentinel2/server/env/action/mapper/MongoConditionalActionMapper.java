package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.ConditionalAction;
import net.poundex.sentinel2.server.env.action.MongoConditionalAction;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoConditionalActionMapper extends AbstractMongoMapper<ConditionalAction, MongoConditionalAction> {
	
	public MongoConditionalActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public ConditionalAction to(MongoConditionalAction obj) {
		return ConditionalAction.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.action(toOther(obj.getAction()))
				.predicate(toOther(obj.getPredicate()))
				.build();
	}

	@Override
	public MongoConditionalAction from(ConditionalAction obj) {
		return MongoConditionalAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.action(fromOther(obj.getAction()))
				.predicate(fromOther(obj.getPredicate()))
				.build();
	}
}
