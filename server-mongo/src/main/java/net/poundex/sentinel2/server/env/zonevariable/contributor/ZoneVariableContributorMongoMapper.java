package net.poundex.sentinel2.server.env.zonevariable.contributor;

import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
class ZoneVariableContributorMongoMapper extends AbstractMongoMapper<ZoneVariableContributor, MongoZoneVariableContributor> {
	
	public ZoneVariableContributorMongoMapper(MapperService mapperService) {
		super(mapperService);
	}

	public MongoZoneVariableContributor from(ZoneVariableContributor obj) {
		return MongoZoneVariableContributor.builder()
				.id(id(obj.getId()))
				.zone(fromOther(obj.getZone()))
				.portId(obj.getPortId().toString())
				.zoneVariable(fromOther(obj.getZoneVariable()))
				.build();
	}

	public ZoneVariableContributor to(MongoZoneVariableContributor obj) {
		return ZoneVariableContributor.builder()
				.id(id(obj.getId()))
				.zone(toOther(obj.getZone()))
				.portId(URI.create(obj.getPortId()))
				.zoneVariable(toOther(obj.getZoneVariable()))
				.build();
	}

}
