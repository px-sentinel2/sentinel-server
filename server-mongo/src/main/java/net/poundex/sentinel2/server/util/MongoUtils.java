package net.poundex.sentinel2.server.util;

import io.vavr.collection.Stream;
import io.vavr.control.Try;
import org.bson.types.ObjectId;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Function;

public interface MongoUtils {
	default <T> Mono<T> withValidId(String rawId, Function<ObjectId, Mono<T>> fn) {
		return Try.of(() -> new ObjectId(rawId))
				.map(fn)
				.recover(IllegalArgumentException.class, Mono.empty())
				.get();
	}
	
	default <T> Flux<T> withValidIds(Iterable<String> rawIds, Function<Iterable<ObjectId>, Flux<T>> fn) {
		return fn.apply(Stream.ofAll(rawIds)
				.map(id -> Try.of(() -> new ObjectId(id)))
				.filter(Try::isSuccess)
				.map(Try::get)
				.toSet());
	}
}
