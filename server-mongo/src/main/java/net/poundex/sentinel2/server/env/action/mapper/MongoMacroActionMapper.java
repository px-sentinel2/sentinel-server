package net.poundex.sentinel2.server.env.action.mapper;

import net.poundex.sentinel2.server.env.action.AbstractMongoAction;
import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.env.action.MacroAction;
import net.poundex.sentinel2.server.env.action.MongoMacroAction;
import net.poundex.sentinel2.server.mapper.AbstractMongoMapper;
import net.poundex.sentinel2.server.mapper.MapperService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
class MongoMacroActionMapper extends AbstractMongoMapper<MacroAction, MongoMacroAction> {
	
	public MongoMacroActionMapper(MapperService mapperService) {
		super(mapperService);
	}

	@Override
	public MacroAction to(MongoMacroAction obj) {
		return MacroAction.builder()
				.id(obj.getId().toString())
				.name(obj.getName())
				.actions(obj.getActions().stream().map(ma -> (Action) toOther(ma)).toList())
				.build();
	}

	@Override
	public MongoMacroAction from(MacroAction obj) {
		return MongoMacroAction.builder()
				.id(new ObjectId(obj.getId()))
				.name(obj.getName())
				.actions(obj.getActions().stream().map(a -> (AbstractMongoAction) fromOther(a)).toList())
				.build();
	}
}
