package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.predicate.AbstractMongoEnvironmentPredicate;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MongoConditionalAction extends AbstractMongoAction {
	private AbstractMongoEnvironmentPredicate predicate;
	private AbstractMongoAction action;
}
