package net.poundex.sentinel2.server.device

import net.poundex.sentinel2.server.messaging.Subscribable
import org.springframework.beans.factory.ObjectProvider
import org.springframework.core.env.Environment
import reactor.core.publisher.Sinks
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Consumer

class DeviceServiceSpec extends Specification {

	Sinks.Many<Device> deviceAddedEventSink = Sinks.many().unicast().onBackpressureError()
	Device someDevice = Stub() {
		getDeviceId() >> URI.create("device://some-device/self")
	}
	
	Driver driver = Mock()
	
	ObjectProvider<Driver> driversProvider = Stub() {
		stream() >> [driver].stream()
	}

	Consumer consumer
	DriverEvents driverEvents = Mock() {
		deviceAdded() >> ({ consumer = it } as Subscribable)
		deviceAdded(_ as Device) >> { Device d -> deviceAddedEventSink.tryEmitNext(d) }
	}

	Environment environment = Stub() {
		getProperty(_, Boolean, true) >> true
	}
	
	@Subject
	DeviceService deviceService = new DeviceService(driversProvider, driverEvents, environment)
	
	void setup() {
		deviceService.init()
	}
	
	void "Return registered devices"() {
		expect:
		deviceService.allDevices.empty
		
		when:
		consumer.accept(someDevice)
		
		then:
		deviceService.allDevices.size() == 1
		deviceService.allDevices.first() == someDevice
	}
	
	void "Start all drivers on reset"() {
		when:
		deviceService.reset()
		
		then:
		1 * driver.start()
	}
}
