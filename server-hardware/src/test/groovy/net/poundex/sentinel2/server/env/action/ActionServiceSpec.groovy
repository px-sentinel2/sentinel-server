package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import spock.lang.Specification
import spock.lang.Subject

class ActionServiceSpec extends Specification {
	
	SentinelEnvironment sentinelEnvironment = Stub()
	
	@Subject
	ActionService service = new ActionService(sentinelEnvironment)
	
	void "Runs action"() {
		given:
		Action action = Mock()
		
		when:
		service.run(action)
		
		then:
		1 * action.run(sentinelEnvironment)
	}
}
