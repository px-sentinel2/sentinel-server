package net.poundex.sentinel2.server.device;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
@RequiredArgsConstructor
class DeviceService implements DeviceManager, ApplicationListener<ApplicationReadyEvent> {
	
	private final ObjectProvider<Driver> drivers;
	private final DriverEvents driverEvents;
	private final Environment springEnvironment;
	
	private final Map<URI, Device> devices = new ConcurrentHashMap<>();

	@PostConstruct
	public void init() {
		driverEvents.deviceAdded().subscribe(this::onDeviceAdded);
	}
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		reset();
	}

	public void reset() {
		devices.clear();
		drivers.stream()
				.filter(driver -> springEnvironment.getProperty(String.format("sentinel.%s.enabled", driver.getName()), 
						Boolean.class, true))
				.forEach(Driver::start);
	}

	public Collection<Device> getAllDevices() {
		return Collections.unmodifiableCollection(devices.values());
	}
	
	private void onDeviceAdded(Device device) {
		log.info("Device added: {}", device);
		devices.put(device.getDeviceId(), device);
	}
	
	@Override
	public Optional<Device> getDevice(URI deviceId) {
		return Optional.ofNullable(devices.get(deviceId));
	}
}
