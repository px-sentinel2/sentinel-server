package net.poundex.sentinel2.server.env.action;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.SentinelEnvironment;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
class ActionService implements ActionManager {
	
	private final SentinelEnvironment sentinelEnvironment;

	@Override
	public void run(Action action) {
		log.info("Running action: {}", action);
		action.run(sentinelEnvironment);
	}
}
